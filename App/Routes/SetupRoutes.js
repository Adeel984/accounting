import NavMenuIc from "../SharedViews/NavMenuIc";
import HeaderButton from "../SharedViews/HeaderButton";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Colors } from "../Shared/Colors";
import Customers from "../Screens/Customers";
import AddCustomer from "../Screens/AddCustomer";
import Vendors from "../Screens/Vendors";
import AddVendor from "../Screens/AddVendor";
import Products from "../Screens/Products";
import AddProduct from "../Screens/AddProduct";
import Taxes from "../Screens/Taxes";
import AddTax from "../Screens/AddTax";
import WareHouses from "../Screens/Warehouses";
import AddWarehouse from "../Screens/AddWarehouse";
import AdjustmentTypes from "../Screens/AdjustmentTypes";
import FeatureNotImplemented from "../Screens/FeatureNotImplemented";
import CustomCategories from "../Screens/CustomCategories";
import AddCustomerCategory from "../Screens/AddCustomerCategory";
import ProductCategories from "../Screens/ProductCategories";
import AddProductCategory from "../Screens/AddProductCategory";

const Stack = createStackNavigator();
const Add = "Add";

export function customersStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Customers"
        component={Customers}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Customer")}
            />
          ),
        })}
      />

      <Stack.Screen name="Customer" component={AddCustomer} />
      {/* <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} /> */}
    </Stack.Navigator>
  );
}

export function vendorStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Vendors"
        component={Vendors}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Vendor")}
            />
          ),
        })}
      />

      <Stack.Screen name="Vendor" component={AddVendor} />
      {/* <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} /> */}
    </Stack.Navigator>
  );
}

export function productStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Products"
        component={Products}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Product")}
            />
          ),
        })}
      />

      <Stack.Screen name="Add Product" component={AddProduct} />
      {/* <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} /> */}
    </Stack.Navigator>
  );
}

export function taxesStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Taxes"
        component={Taxes}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Tax")}
            />
          ),
        })}
      />

      <Stack.Screen name="Add Tax" component={AddTax} />
    </Stack.Navigator>
  );
}

export function warehouseStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Warehouses"
        component={WareHouses}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Warehouse")}
            />
          ),
        })}
      />

      <Stack.Screen name="Add Warehouse" component={AddWarehouse} />
    </Stack.Navigator>
  );
}

export function adjustmentTypesStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Adjustment Types"
        component={AdjustmentTypes}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Adjustment Type")}
            />
          ),
        })}
      />

      <Stack.Screen name="Adjustment Type" component={FeatureNotImplemented} />
    </Stack.Navigator>
  );
}

export function customCategoriesStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Customer Categories"
        component={CustomCategories}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Customer Category")}
            />
          ),
        })}
      />

      <Stack.Screen
        name="Add Customer Category"
        component={AddCustomerCategory}
      />
    </Stack.Navigator>
  );
}

export function productCategoriesStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Product Categories"
        component={ProductCategories}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Product Category")}
            />
          ),
        })}
      />

      <Stack.Screen
        name="Add Product Category"
        component={AddProductCategory}
      />
    </Stack.Navigator>
  );
}

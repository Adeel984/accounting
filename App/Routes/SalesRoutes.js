import NavMenuIc from "../SharedViews/NavMenuIc";
import HeaderButton from "../SharedViews/HeaderButton";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Colors } from "../Shared/Colors";
import AddSaleQuotation from "../Screens/AddSaleQuotation";
import SaleQuotation from "../Screens/SaleQuotation";
import SaleOrders from "../Screens/SaleOrders";
import AddSaleOrder from "../Screens/AddSaleOrder";
import AddProductDetail from "../Screens/AddProductDetail";
import SaleDelivery from "../Screens/SaleDelivery";
import AddSaleDelivery from "../Screens/AddSaleDelivery";
import AddProductDetailSaleDelivery from "../Screens/AddProductDetailSaleDelivery";
import SaleInvoices from "../Screens/SaleInvoices";
import AddSaleInvoice from "../Screens/AddSaleInvoice";
import Returns from "../Screens/Returns";
import AddSaleReturn from "../Screens/AddSaleReturn";
import ReceiveMoney from "../Screens/ReceiveMoney";
import AddReceiveMoney from "../Screens/AddReceiveMoney";
import AddCustomerRefund from "../Screens/AddCustomerRefund";
import FeatureNotImplemented from "../Screens/FeatureNotImplemented";
import Refunds from "../Screens/Refunds";
const Stack = createStackNavigator();

const Add = "ADD";

export function QuotationStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Sale Quotations"
        component={SaleQuotation}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Sale Quotation")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Sale Quotation" component={AddSaleQuotation} />
    </Stack.Navigator>
  );
}

export function OrderStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Sale Orders"
        component={SaleOrders}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Sale Order")}
            />
          ),
        })}
      />

      <Stack.Screen name="Add Sale Order" component={AddSaleOrder} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function DeliveryStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Sale Delivery"
        component={SaleDelivery}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Sale Delivery")}
            />
          ),
        })}
      />

      <Stack.Screen name="Add Sale Delivery" component={AddSaleDelivery} />
      <Stack.Screen
        name="Add Product Detail"
        component={AddProductDetailSaleDelivery}
      />
    </Stack.Navigator>
  );
}

export function InvoiceStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Sale Invoices"
        component={SaleInvoices}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Sale Invoice")}
            />
          ),
        })}
      />

      <Stack.Screen name="Sale Invoice" component={AddSaleInvoice} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function RecurringStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Recurring Invoice"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Sale Invoice")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Sale Invoice" component={AddSaleInvoice} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function ReturnStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Sale Returns"
        component={Returns}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Sale Return")}
            />
          ),
        })}
      />

      <Stack.Screen name="Add Sale Return" component={AddSaleReturn} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function ReceiveMoneyStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Receive Money"
        component={ReceiveMoney}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Add Receive Money")}
            />
          ),
        })}
      />

      <Stack.Screen name="Add Receive Money" component={AddReceiveMoney} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function CustomerRefundStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Customer Refunds"
        component={Refunds}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Customer Refund")}
            />
          ),
        })}
      />

      <Stack.Screen name="Customer Refund" component={AddCustomerRefund} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function CustomerSettlementStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Customer Settlements"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Customer Refund")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Customer Refund" component={AddCustomerRefund} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

import NavMenuIc from "../SharedViews/NavMenuIc";
import HeaderButton from "../SharedViews/HeaderButton";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Colors } from "../Shared/Colors";
import Expenses from "../Screens/Expenses";
import AddExpense from "../Screens/AddExpense";
import AddExpenseDetail from "../Screens/AddExpenseDetail";
import FeatureNotImplemented from "../Screens/FeatureNotImplemented";
import BankAccounts from "../Screens/BankAccounts";
import AddBankAccount from "../Screens/AddBankAccount";

const Stack = createStackNavigator();

const Add = "Add";

export function accountsExpenseStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Expenses"
        component={Expenses}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Expense")}
            />
          ),
        })}
      />

      <Stack.Screen name="Expense" component={AddExpense} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

export function journalEntryStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Journal Entries"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Expense")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Expense" component={AddExpense} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

export function chartOfAccountStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Chart of Account"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Expense")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Expense" component={AddExpense} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

export function bankAccountStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Bank Accounts"
        component={BankAccounts}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Bank Account")}
            />
          ),
        })}
      />

      <Stack.Screen name="Bank Account" component={AddBankAccount} />
    </Stack.Navigator>
  );
}

export function bankDepositStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Bank Deposits"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Expense")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Bank Deposit" component={AddExpense} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

export function creditNoteStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Credit Notes"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Expense")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Bank Deposit" component={AddExpense} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

export function debitNoteStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Debit Notes"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Expense")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Bank Deposit" component={AddExpense} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

export function fundsTransferStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Funds Transfers"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Expense")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Bank Deposit" component={AddExpense} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

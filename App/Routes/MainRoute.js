import React, { useCallback, useEffect, useState } from "react";
import * as SplashScreen from "expo-splash-screen";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "../Screens/LoginScreen";
import { GetData } from "../Shared/General";
import DrawerScreen from "../Screens/DrawerScreen";
import { NavigationContainer } from "@react-navigation/native";
const Stack = createStackNavigator();

function MainRoute() {
  const [appIsReady, setAppIsReady] = useState(false);
  const [isLoggedin, setIsLoggedin] = useState(false);

  useEffect(() => {
    async function prepare() {
      try {
        await SplashScreen.preventAutoHideAsync();
        var data = await GetData("login");
        if (data != null) {
          setIsLoggedin(true);
          console.log("Loggedin");
        } else console.log("Data is null");
        //  await new Promise((resolve) => setTimeout(resolve, 5000));
      } catch (e) {
      } finally {
        setAppIsReady(true);
      }
    }
    prepare();
  }, []);

  const onLayoutRootView = useCallback(async () => {
    if (appIsReady) {
      await SplashScreen.hideAsync();
    }
  }, [appIsReady]);

  onLayoutRootView();
  if (!appIsReady) return null;
  console.log(isLoggedin);
  const initialRouteName = isLoggedin ? "homeDrawer" : "login";
  console.log(initialRouteName);

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={initialRouteName}
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name="homeDrawer" component={DrawerScreen} />

        <Stack.Screen name="login" component={LoginScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default MainRoute;

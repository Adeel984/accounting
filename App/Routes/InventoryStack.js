import NavMenuIc from "../SharedViews/NavMenuIc";
import HeaderButton from "../SharedViews/HeaderButton";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Colors } from "../Shared/Colors";
import AddTransferOut from "../Screens/AddTransferOut";
import TransferOut from "../Screens/TransferOut";
import TransferIn from "../Screens/TransferIn";
import FeatureNotImplemented from "../Screens/FeatureNotImplemented";
import AddExpenseDetail from "../Screens/AddExpenseDetail";

const Stack = createStackNavigator();
const Add = "Add";

export function stockAdjustmentsStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Stock Adjustments"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Expense")}
          //   />
          // ),
        })}
      />

      {/* <Stack.Screen name="Bank Deposit" component={AddExpense} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} /> */}
    </Stack.Navigator>
  );
}

export function transferOutStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Transfer Outs"
        component={TransferOut}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Transfer Out")}
            />
          ),
        })}
      />

      <Stack.Screen name="Transfer Out" component={AddTransferOut} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

export function transferInStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Transfer Ins"
        component={TransferIn}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Transfer In")}
            />
          ),
        })}
      />

      <Stack.Screen name="Transfer In" component={AddTransferOut} />
      <Stack.Screen name="Add Expense Detail" component={AddExpenseDetail} />
    </Stack.Navigator>
  );
}

import NavMenuIc from "../SharedViews/NavMenuIc";
import HeaderButton from "../SharedViews/HeaderButton";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Colors } from "../Shared/Colors";
import PurchaseOrders from "../Screens/PurchaseOrders";
import AddPurchaseOrder from "../Screens/AddPurchaseOrder";
import AddProductDetail from "../Screens/AddProductDetail";
import GoodsReceiving from "../Screens/GoodsReceiving";
import AddGoodsReceiving from "../Screens/AddGoodsReceiving";
import PurchaseInvoices from "../Screens/PurchaseInvoices";
import AddPurchaseInvoice from "../Screens/AddPurchaseInvoice";
import PurchaseReturns from "../Screens/PurchaseReturns";
import AddPurchaseReturn from "../Screens/AddPurchaseReturn";
import MakePayments from "../Screens/MakePayments";
import AddMakePayment from "../Screens/AddMakePayment";
import VendorRefund from "../Screens/VendorRefund";
import AddVendorRefund from "../Screens/AddVendorRefund";
import FeatureNotImplemented from "../Screens/FeatureNotImplemented";

const Stack = createStackNavigator();

const Add = "Add";

export function purchaseOrdersStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Purchase Orders"
        component={PurchaseOrders}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Purchase Order")}
            />
          ),
        })}
      />

      <Stack.Screen name="Purchase Order" component={AddPurchaseOrder} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function goodsRecevingStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Goods Receivings"
        component={GoodsReceiving}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Goods Receiving")}
            />
          ),
        })}
      />

      <Stack.Screen name="Goods Receiving" component={AddGoodsReceiving} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function purchaseInvoiceStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Purchase Invoices"
        component={PurchaseInvoices}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Purchase Invoice")}
            />
          ),
        })}
      />

      <Stack.Screen name="Purchase Invoice" component={AddPurchaseInvoice} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function purchaseReturnStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Purchase Returns"
        component={PurchaseReturns}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Purchase Return")}
            />
          ),
        })}
      />

      <Stack.Screen name="Purchase Return" component={AddPurchaseReturn} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function makePaymentStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Make Payments"
        component={MakePayments}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Make Payment")}
            />
          ),
        })}
      />

      <Stack.Screen name="Make Payment" component={AddMakePayment} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function paymentRefundStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Vendor Refunds"
        component={VendorRefund}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          headerRight: () => (
            <HeaderButton
              title={Add}
              onPress={() => navigation.navigate("Vendor Refund")}
            />
          ),
        })}
      />

      <Stack.Screen name="Vendor Refund" component={AddVendorRefund} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

export function paymentSettlementStack({ navigation }) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
      }}
    >
      <Stack.Screen
        name="Vendor Settlements"
        component={FeatureNotImplemented}
        options={navigation.options}
        options={({ navigation }) => ({
          headerLeft: () => <NavMenuIc navigation={navigation} />,
          // headerRight: () => (
          //   <HeaderButton
          //     title={Add}
          //     onPress={() => navigation.navigate("Vendor Refund")}
          //   />
          // ),
        })}
      />

      <Stack.Screen name="Vendor Refund" component={AddVendorRefund} />
      <Stack.Screen name="Add Product Detail" component={AddProductDetail} />
    </Stack.Navigator>
  );
}

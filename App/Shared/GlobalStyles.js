import { StyleSheet } from "react-native";
import { Colors } from "./Colors";

export const GlobalStyles = StyleSheet.create({
  card: {
    borderRadius: 6,
    elevation: 3,
    shadowColor: Colors.separator,
    backgroundColor: Colors.white,
    shadowOffset: { width: 2.5, height: 2.5 },
    shadowOpacity: 0.7,
    shadowRadius: 5,
    marginHorizontal: 4,
    borderColor: Colors.separator,
    borderWidth: 1,
    marginVertical: 6,
  },
  container: {
    justifyContent: "center",
    flex: 1,
    marginHorizontal: 20,
  },
  flexDirRow: {
    flexDirection: "row",
  },
  flex1: {
    flex: 1,
  },
  outSideContainer: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  placeholder: {
    position: "absolute",
    backgroundColor: "white",
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  separator: {
    height: 1,
    backgroundColor: Colors.separator,
  },
  lineBlack: {
    height: 1,
    backgroundColor: Colors.black,
  },
});

import { Colors } from "../Shared/Colors";

export const GlobalOptions = {
  headerStyle: {
    backgroundColor: Colors.main,
  },
  headerShown: true,
  headerTintColor: Colors.white,
  headerTitleStyle: {},
};

import React from 'react';
import { AntDesign } from '@expo/vector-icons';
import { Colors } from '../Shared/Colors';

function CaretDown() {
    return (
        <AntDesign 
        name="caretdown" 
        size={14}
        style={{textAlign:'right', marginRight:7, alignSelf:'center'}} 
        color={Colors.black} />
    );
}

export default CaretDown;
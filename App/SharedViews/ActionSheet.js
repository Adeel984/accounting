import PropTypes from "prop-types";
import React, { createRef } from "react";
import { StyleSheet, Text, TouchableHighlight, View } from "react-native";
import { Colors } from "../Shared/Colors";
import ActionSheet from "react-native-actions-sheet";
import { GlobalStyles } from "../Shared/GlobalStyles";

const PRIMARY_COLOR = "rgb(0,98,255)";
const BORDER_COLOR = "#DBDBDB";
const actionSheetRef = createRef();

const ActionSheetView = (props) => {
  const { actionItems } = props;
  const actionSheetItems = [
    ...actionItems,
    {
      id: "#cancel",
      label: "Cancel",
      onPress: props?.onCancel,
    },
  ];
  return (
    <View style={GlobalStyles.flex1}>
      <ActionSheet
        ref={actionSheetRef}
        closable={true}
        closeOnTouchBackdrop={true}
      >
        <View style={styles.modalContent}>
          {actionSheetItems.map((actionItem, index) => {
            return (
              <TouchableHighlight
                style={[
                  styles.actionSheetView,
                  index === 0 && {
                    borderTopLeftRadius: 12,
                    borderTopRightRadius: 12,
                  },
                  index === actionSheetItems.length - 2 && {
                    borderBottomLeftRadius: 12,
                    borderBottomRightRadius: 12,
                  },
                  index === actionSheetItems.length - 1 && {
                    borderBottomWidth: 0,
                    backgroundColor: Colors.white,
                    marginTop: 8,
                    borderTopLeftRadius: 12,
                    borderTopRightRadius: 12,
                    borderBottomLeftRadius: 12,
                    borderBottomRightRadius: 12,
                  },
                ]}
                underlayColor={"#f7f7f7"}
                key={index}
                onPress={actionItem.onPress}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.actionSheetText,
                    props?.actionTextColor && {
                      color: props?.actionTextColor,
                    },
                    index === actionSheetItems.length - 1 && {
                      color: "#fa1616",
                    },
                  ]}
                >
                  {actionItem.label}
                </Text>
              </TouchableHighlight>
            );
          })}
        </View>
      </ActionSheet>
    </View>
  );
};

const styles = StyleSheet.create({
  actionSheetText: {
    fontSize: 18,
    color: PRIMARY_COLOR,
  },
  actionSheetView: {
    backgroundColor: Colors.white,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 16,
    paddingBottom: 16,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: BORDER_COLOR,
  },
  modalContent: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 20,
    justifyContent: "flex-end",
    flex: 1,
  },
});

ActionSheetView.propTypes = {
  actionItems: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      label: PropTypes.string,
      onPress: PropTypes.func,
    })
  ).isRequired,
  onCancel: PropTypes.func,
  onShow: PropTypes.func,
  actionTextColor: PropTypes.string,
};

ActionSheetView.defaultProps = {
  actionItems: [],
  onCancel: () => {},
  actionTextColor: null,
  onShow: () => {
    console.log("OnShowing");
    console.log(actionSheetRef.current === null);
    actionSheetRef.current?.show();
  },
};

export default ActionSheetView;

import React from "react";

import { StyleSheet } from "react-native";
import { FAB } from "react-native-paper";
import { Colors } from "../Shared/Colors";

function FabButton({ onPress }) {
  return (
    <FAB
      style={styles.fab}
      icon="plus"
      color={Colors.white}
      onPress={onPress}
    />
  );
}

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: Colors.main,
  },
});

export default FabButton;

import React from 'react';
import {ColorPropType, StyleSheet, View} from 'react-native';
import { Colors } from '../Shared/Colors';

function CardView(props) {
    return (
        <View style={styles.card}>
            <View style={styles.cardContent}>
                {props.children}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 6,
        elevation: 3,
        shadowColor:Colors.separator,
        backgroundColor: Colors.white,
        shadowOffset : {width: 2.5, height:2.5},
        shadowOpacity: 0.7,
        shadowRadius: 5,
        marginHorizontal:4,
        borderColor:Colors.separator,
        borderWidth:1,
        marginVertical:6,

    },
    cardContent : {
        margin:10,
    }
});

export default CardView;
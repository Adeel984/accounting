import React from "react";
import {
  TouchableOpacity,
  StyleSheet,
  TouchableHighlight,
  Platform,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Colors } from "../Shared/Colors";
import { Button, IconButton } from "react-native-paper";

function NavMenuIc({ navigation }) {
  var ic = Platform.OS === "android" ? "arrow-left" : "chevron-left";
  var size = Platform.OS === "android" ? 24 : 35;
  return (
    <IconButton
      icon={ic}
      size={size}
      style={{ marginLeft: 3 }}
      onPress={() => {
        if (navigation.canGoBack()) navigation.navigate("home");
      }}
      color={Colors.white}
    />
  );
}
const styles = StyleSheet.create({
  icon: {
    fontSize: 26,
    marginLeft: 10,
    color: Colors.white,
  },
});

export default NavMenuIc;

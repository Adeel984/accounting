import React from 'react';
import { View,StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Colors } from '../Shared/Colors';

function FlatButton({text, onPress, backgroundColor}) { 
    if(backgroundColor === undefined) backgroundColor = Colors.main;
    
    return (
        <TouchableOpacity onPress={onPress} activeOpacity={0.85}>
        <View style={[styles.button, { backgroundColor:backgroundColor }]}>
            <Text style={styles.buttonText}>{text}</Text>
        </View>
        </TouchableOpacity>
    );
}
const styles = StyleSheet.create({
    button:{
        borderRadius:8,
        paddingVertical:14,
        paddingHorizontal:10,
        height:50,
        marginTop:10, 
        backgroundColor:Colors.main,
    },
    buttonText:{
        color:'white',
        textAlign:'center',
        fontSize:16
    }
    
})

export default FlatButton;
import React from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";
import { Colors } from "../Shared/Colors";

function HeaderButton({ title, onPress }) {
  return (
    <TouchableOpacity
      activeOpacity={0.75}
      style={styles.buttonStyle}
      onPress={onPress}
    >
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  text: {
    color: Colors.white,
    fontSize: 16,
  },
  buttonStyle: {
    width: 40,
    height: 30,
    marginHorizontal: 10,
    justifyContent: "center",
  },
});

export default HeaderButton;

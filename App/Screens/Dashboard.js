import React from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
} from "react-native";
import { GlobalStyles } from "../Shared/GlobalStyles";
import CardView from "../SharedViews/CardView";
import { LinearGradient } from "expo-linear-gradient";
import { MaterialCommunityIcons, FontAwesome } from "@expo/vector-icons";
import { Colors } from "../Shared/Colors";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";

const data = [
  {
    name: "Over Due",
    amount: 5000,
    color: Colors.pink,
    legendFontColor: "#7F7F7F",
    legendFontSize: 15,
  },
  {
    name: "Invoiced",
    amount: 5000,
    color: Colors.yellow,
    legendFontColor: "#7F7F7F",
    legendFontSize: 15,
  },
];

const accountReceivableAging = {
  labels: ["January", "February", "March", "April", "May", "June"],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43],
    },
  ],
};

const chartConfig = {
  backgroundColor: "#e26a00",
  backgroundGradientFrom: "#fb8c00",
  backgroundGradientTo: "#ffa726",
  decimalPlaces: 2, // optional, defaults to 2dp
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
  labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
};

function Dashboard({ navigation }) {
  //navigation.openDrawer();
  return (
    <SafeAreaView style={GlobalStyles.flex1}>
      <StatusBar backgroundColor={Colors.primary} />
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.headingText}>Sales</Text>

          <View style={styles.line} />

          <View style={styles.backgroundView}>
            <CardView>
              <View style={styles.cardViewContent}>
                <LinearGradient
                  colors={[Colors.purple1, Colors.purple2]}
                  style={styles.gradient}
                >
                  <MaterialCommunityIcons name="calendar" style={styles.icon} />
                </LinearGradient>

                <View style={styles.viewLeft}>
                  <Text style={styles.textContent}>Today</Text>

                  <Text style={styles.number}>0</Text>
                </View>

                <View style={styles.viewRight}>
                  <Text style={styles.textContentRight}>Yesterday</Text>

                  <Text style={styles.numberRight}>0</Text>
                </View>

                <FontAwesome
                  name="long-arrow-up"
                  size={30}
                  color={Colors.green}
                />
              </View>
            </CardView>

            <CardView>
              <View style={styles.cardViewContent}>
                <LinearGradient
                  colors={[Colors.green, Colors.green2]}
                  style={styles.gradient}
                >
                  <MaterialCommunityIcons
                    name="calendar-check"
                    style={styles.icon}
                  />
                </LinearGradient>

                <View style={styles.viewLeft}>
                  <Text style={styles.textContent}>This Week</Text>

                  <Text style={styles.number}>0</Text>
                </View>

                <View style={styles.viewRight}>
                  <Text style={styles.textContentRight}>Last Week</Text>

                  <Text style={styles.numberRight}>0</Text>
                </View>

                <FontAwesome
                  name="long-arrow-up"
                  size={30}
                  color={Colors.green}
                />
              </View>
            </CardView>

            <CardView>
              <View style={styles.cardViewContent}>
                <LinearGradient
                  colors={[Colors.blue1, Colors.blue2]}
                  style={styles.gradient}
                >
                  <MaterialCommunityIcons
                    name="calendar-month"
                    style={styles.icon}
                  />
                </LinearGradient>

                <View style={styles.viewLeft}>
                  <Text style={styles.textContent}>This Month</Text>

                  <Text style={styles.number}>0</Text>
                </View>

                <View style={styles.viewRight}>
                  <Text style={styles.textContentRight}>Last Month</Text>

                  <Text style={styles.numberRight}>0</Text>
                </View>

                <FontAwesome
                  name="long-arrow-down"
                  size={30}
                  color={Colors.red}
                />
              </View>
            </CardView>

            <CardView>
              <View style={styles.cardViewContent}>
                <LinearGradient
                  colors={[Colors.peach1, Colors.peach2]}
                  style={styles.gradient}
                >
                  <MaterialCommunityIcons
                    name="calendar-blank"
                    style={styles.icon}
                  />
                </LinearGradient>

                <View style={styles.viewLeft}>
                  <Text style={styles.textContent}>This Year</Text>

                  <Text style={styles.number}>5,000</Text>
                </View>

                <View style={styles.viewRight}>
                  <Text style={styles.textContentRight}>Last Year</Text>

                  <Text style={styles.numberRight}>0</Text>
                </View>

                <FontAwesome
                  name="long-arrow-up"
                  size={30}
                  color={Colors.green}
                />
              </View>
            </CardView>
          </View>

          <View style={styles.backgroundBottom}>
            <CardView style={styles.fixedCardView}>
              <Text style={styles.headingText}>Cash and Banks</Text>
              <View style={styles.line} />

              <View style={styles.table}>
                <View style={styles.grayedBg}>
                  <Text style={styles.headingLeft}>Name</Text>

                  <Text style={styles.headingRight}>Amount</Text>
                </View>

                <View style={styles.tableContent}>
                  <Text style={styles.tableContentLeftText}>Cash in hand</Text>
                  <Text style={styles.tableContentRightText}>Rs 0</Text>
                </View>

                <View style={styles.tableContent}>
                  <Text style={styles.tableContentLeftText}>
                    Undeposited Funds
                  </Text>
                  <Text style={styles.tableContentRightText}>Rs 0</Text>
                </View>

                <View style={styles.tableContent}>
                  <Text style={styles.tableContentLeftText}>Jawed Trading</Text>
                  <Text style={styles.tableContentRightText}>Rs 0</Text>
                </View>
              </View>
            </CardView>

            {/* End of Cash and banks */}

            {/* Start of Invoices */}

            <CardView style={styles.fixedCardView}>
              <Text style={styles.headingText}>Invoices</Text>
              <View style={styles.line} />

              <View style={styles.pieHeadView}>
                <View
                  style={[styles.roundView, { backgroundColor: Colors.yellow }]}
                />
                <Text style={styles.roundViewText}>Invoiced</Text>

                <View
                  style={[styles.roundView, { backgroundColor: Colors.pink }]}
                />
                <Text style={styles.roundViewText}>Over Due</Text>

                <View
                  style={[
                    styles.roundView,
                    { backgroundColor: Colors.lightGreen },
                  ]}
                />
                <Text style={styles.roundViewText}>Payment</Text>
              </View>

              <PieChart
                data={data}
                width={420}
                height={320}
                style={{ marginBottom: 30 }}
                chartConfig={chartConfig}
                accessor={"amount"}
                backgroundColor={"transparent"}
                paddingLeft={"15"}
                center={[10, 50]}
                absolute
              />
            </CardView>

            {/* End of Invoices */}

            {/* Start of Top Customers */}

            <CardView style={styles.fixedCardView}>
              <Text style={styles.headingText}>Top Customers</Text>
              <View style={styles.line} />

              <View style={styles.pieHeadView}>
                <View
                  style={[
                    styles.roundView,
                    { backgroundColor: Colors.purple1 },
                  ]}
                />
                <Text style={styles.roundViewText}>Raza</Text>

                <View
                  style={[styles.roundView, { backgroundColor: Colors.blue2 }]}
                />
                <Text style={styles.roundViewText}>Other Customers</Text>
              </View>
            </CardView>

            {/* End of Top Customers */}

            {/* Start of Account Receiveable */}

            <CardView style={styles.fixedCardView}>
              <Text style={styles.headingText}>Account Receivable Aging</Text>
              <View style={styles.line} />

              <BarChart
                style={styles.graphStyle}
                data={accountReceivableAging}
                width={300}
                height={220}
                yAxisLabel="$"
                chartConfig={chartConfig}
                verticalLabelRotation={30}
              />
            </CardView>

            {/* End of Account Receiveable */}

            {/* Start of Expenses */}

            <CardView style={styles.fixedCardView}>
              <Text style={styles.headingText}>Expenses</Text>
              <View style={styles.line} />

              <View
                style={[
                  styles.pieHeadView,
                  { justifyContent: "flex-start", marginLeft: 30 },
                ]}
              >
                <View
                  style={[styles.roundView, { backgroundColor: Colors.green }]}
                />
                <Text style={styles.roundViewText}>General Expenses</Text>

                <View
                  style={[styles.roundView, { backgroundColor: Colors.red }]}
                />
                <Text style={styles.roundViewText}>Discount Received</Text>
              </View>

              <View
                style={[
                  styles.pieHeadView,
                  { justifyContent: "flex-start", marginLeft: 30 },
                ]}
              >
                <View
                  style={[styles.roundView, { backgroundColor: Colors.blue2 }]}
                />
                <Text style={styles.roundViewText}>Other Expense Accounts</Text>
              </View>
            </CardView>

            {/* End of Expenses */}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  backgroundView: {
    backgroundColor: "#ececec",
    marginHorizontal: -8,
    padding: 3,
    borderRadius: 10,
  },
  backgroundBottom: {
    backgroundColor: "#ececec",
    marginHorizontal: -10,
    marginTop: 6,
  },
  cardViewContent: {
    flexDirection: "row",
  },
  container: {
    flex: 1,
    margin: 10,
    marginTop: 20,
  },
  fixedCardView: {
    marginHorizontal: -20,
  },
  gradient: {
    height: 80,
    width: 100,
    alignItems: "center",
    justifyContent: "center",
  },
  grayedBg: {
    backgroundColor: "#f2f2f2",
    flexDirection: "row",
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  headingLeft: {
    fontWeight: "bold",
    flex: 1,
    fontSize: 14,
  },
  headingRight: {
    fontWeight: "bold",
    fontSize: 14,
  },
  headingText: {
    fontSize: 19,
    marginTop: 5,
  },
  icon: {
    fontSize: 50,
    color: Colors.white,
  },
  line: {
    backgroundColor: Colors.gray,
    height: 1,
    marginHorizontal: -10,
    marginTop: 10,
  },
  number: {
    fontSize: 15,
    color: Colors.darkGray,
    fontWeight: "bold",
    marginLeft: 5,
  },
  numberRight: {
    fontSize: 12,
    color: Colors.black,
    marginLeft: 5,
    marginTop: 4,
    textAlign: "right",
  },
  pieHeadView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 15,
  },
  roundView: {
    height: 16,
    width: 16,
    borderRadius: 8,
    alignSelf: "center",
  },
  roundViewText: {
    marginLeft: 7,
    textAlignVertical: "center",
    fontSize: 14,
    marginRight: 20,
  },
  table: {
    margin: 10,
  },
  tableContent: {
    flexDirection: "row",
    marginVertical: 10,
    marginLeft: 5,
  },
  tableContentRightText: {
    fontSize: 14,
  },
  tableContentLeftText: {
    fontSize: 14,
    flex: 1,
  },
  textContent: {
    fontWeight: "bold",
    fontSize: 15,
    flex: 1,
  },
  textContentRight: {
    fontSize: 12,
  },
  viewLeft: {
    marginLeft: 10,
    flex: 1,
  },
  viewRight: {
    marginHorizontal: 10,
  },
});

export default Dashboard;

import React from "react";
import { View, TextInput, StyleSheet, Text } from "react-native";
import { Colors } from "../Shared/Colors";

function AddCustomerCategory(props) {
  return (
    <View style={styles.container}>
      <View>
        <View style={styles.borderedInput}>
          <TextInput
            style={styles.textInput}
            placeholder="Customer Category Name"
          />
        </View>
        <Text style={styles.placeholder}>Customer Category Name</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  borderedInput: {
    height: 50,
    borderColor: Colors.black,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  placeholder: {
    position: "absolute",
    backgroundColor: "white",
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  container: {
    padding: 15,
    backgroundColor: Colors.white,
    flex: 1,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 7,
  },
});

export default AddCustomerCategory;

import React from "react";
import { View, StyleSheet, Text } from "react-native";

function SplashScreen(props) {
  return (
    <View style={styles.container}>
      <Text>This Is Splash Screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
});

export default SplashScreen;

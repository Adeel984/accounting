import React, { useState } from "react";
import { Picker } from "@react-native-picker/picker";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  Modal,
  Pressable,
  FlatList,
  TouchableWithoutFeedback,
} from "react-native";
import { GlobalStyles } from "../Shared/GlobalStyles";
import { Colors } from "../Shared/Colors";
import FlatButton from "../SharedViews/Button";
import CardView from "../SharedViews/CardView";

function BasicStructure() {
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedType, setSelectedType] = useState();

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.modalView}>
          <Text style={styles.headerText}>Sale Taxes</Text>
          <View style={GlobalStyles.flexDirRow}>
            {/*First Picker */}
            <View style={styles.borderedInputView}>
              <Picker
                selectedValue={selectedType}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedType(itemValue)
                }
              >
                <Picker.Item label="Sale Tax" value="ST" />
                <Picker.Item label="Income Tax" value="INC" />
              </Picker>
            </View>

            {/*2nd  Picker */}

            <View style={styles.borderedInputView}>
              <Picker>
                <Picker.Item label="Sale Price" value="SP" />
              </Picker>
            </View>
            <View style={{ paddingTop: 10, flex: 0.3 }}>
              <FlatButton text="Add" width={100} />
            </View>
          </View>

          <View style={styles.listHeadView}>
            <View style={styles.minusView}>
              <Text>-</Text>
            </View>
            <CardView>
              <Text style={styles.listText}>GST (17 %) - On Sale Price</Text>
            </CardView>
          </View>
        </View>
      </Modal>

      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.textStyle}>Show Modal</Text>
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  borderedInputView: {
    borderColor: Colors.gray,
    borderWidth: 1,
    height: 50,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    flex: 0.5,
  },
  headerText: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
  },
  inputSuggestionView: {
    flexDirection: "row",
  },
  minusView: {
    backgroundColor: "tomato",
    height: 20,
    width: 20,
    borderRadius: 10,
    position: "absolute",
    right: 0,
    zIndex: 100,
  },
  listText: {
    color: Colors.blue2,
    marginVertical: 25,
    fontWeight: "bold",
    marginHorizontal: 10,
    fontSize: 17,
  },
  listHeadView: {
    marginTop: 20,
  },
  modalView: {
    marginVertical: 30,
    backgroundColor: "white",
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    flex: 1,
    shadowOpacity: 0.55,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});

export default BasicStructure;

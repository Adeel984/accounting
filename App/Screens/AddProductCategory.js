import React, { useState } from "react";
import {
  View,
  TextInput,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { Picker } from "@react-native-picker/picker";

function AddProductCategory({ navigation }) {
  const [selectedLanguage, setSelectedLanguage] = useState();
  return (
    <View style={styles.container}>
      <View style={styles.innerView}>
        <TouchableWithoutFeedback>
          <View>
            <View style={styles.borderedInput}>
              <Picker
                selectedValue={selectedLanguage}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedLanguage(itemValue)
                }
              >
                <Picker.Item label="Chemical" value="bank" />
              </Picker>

              {/* <CaretDown/> */}
            </View>
            <Text style={styles.placeholder}>Parent Product Category</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>

      <View>
        <View style={styles.borderedInput}>
          <TextInput
            style={styles.textInput}
            placeholder="Product Category Name"
          />
        </View>
        <Text style={styles.placeholder}>Product Category Name</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  borderedInput: {
    height: 50,
    borderColor: Colors.black,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  placeholder: {
    position: "absolute",
    backgroundColor: "white",
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  container: {
    padding: 10,
    backgroundColor: Colors.white,
    flex: 1,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 7,
  },
});

export default AddProductCategory;

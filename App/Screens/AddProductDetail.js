import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import { MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { Colors } from "../Shared/Colors";
import { GlobalStyles } from "../Shared/GlobalStyles";
import FlatButton from "../SharedViews/Button";
import CaretDown from "../SharedViews/CaretDown";
import CardView from "../SharedViews/CardView";

function AddProductDetail() {
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedType, setSelectedType] = useState();

  const [selectedDiscountType, setSelectedDiscountType] = useState();
  const discountPHolder =
    selectedDiscountType === "percent"
      ? "Discount in %"
      : selectedDiscountType === "amount"
      ? "Discount in amount"
      : "Discount in price";
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.primary} />

      {/* Search View */}
      <View style={styles.searchView}>
        <View style={GlobalStyles.flexDirRow}>
          <TextInput style={styles.searchInput} placeholder="Search Product" />
          <MaterialIcons name="cancel" size={24} color={Colors.darkGray} />
        </View>
        <View style={styles.searchLine} />
      </View>

      {/* Description */}

      <TextInput
        style={[styles.textInput, { height: 80 }]}
        multiline
        placeholder="Description"
      />

      {/* Batch */}
      <View style={[styles.borderedInput, { borderColor: Colors.black }]}>
        <Picker>
          <Picker.Item label="Sale Tax" value="ST" />
          <Picker.Item label="Income Tax" value="INC" />
        </Picker>
      </View>

      {/* Quantity */}

      <TextInput
        style={styles.textInput}
        placeholder="Quantity"
        keyboardType="numeric"
      />

      {/* Price */}
      <TextInput
        style={styles.textInput}
        placeholder="Price"
        keyboardType="numeric"
      />

      {/* Discount in percentage */}

      <View style={GlobalStyles.flexDirRow}>
        <TextInput
          style={[styles.textInput, GlobalStyles.flex1]}
          placeholder={discountPHolder}
          keyboardType="numeric"
        />

        <View style={[styles.percentageView]}>
          <Picker
            style={{ flex: 1 }}
            selectedValue={selectedDiscountType}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedDiscountType(itemValue)
            }
          >
            <Picker.Item label="%" value="percent" />
            <Picker.Item label="amt" value="amount" />
            <Picker.Item label="dp" value="discountPrice" />
          </Picker>

          <View style={GlobalStyles.lineBlack} />
        </View>
      </View>

      <FlatButton text="Add Tax" onPress={() => setModalVisible(true)} />

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.modalView}>
          <View style={GlobalStyles.flex1}>
            <Text style={styles.headerText}>Sale Taxes</Text>
            <View style={GlobalStyles.flexDirRow}>
              {/*First Picker */}
              <View style={styles.borderedInputView}>
                <Picker
                  selectedValue={selectedType}
                  onValueChange={(itemValue, itemIndex) =>
                    setSelectedType(itemValue)
                  }
                >
                  <Picker.Item label="Sale Tax" value="ST" />
                  <Picker.Item label="Income Tax" value="INC" />
                </Picker>
              </View>

              {/*2nd  Picker */}

              <View style={styles.borderedInputView}>
                <Picker>
                  <Picker.Item label="Sale Price" value="SP" />
                </Picker>
              </View>
              <View style={{ paddingTop: 10, flex: 0.3 }}>
                <FlatButton text="Add" width={100} />
              </View>
            </View>

            <View style={styles.listHeadView}>
              <View style={styles.minusView}>
                <Text>-</Text>
              </View>
              <CardView>
                <Text style={styles.listText}>GST (17 %) - On Sale Price</Text>
              </CardView>
            </View>
          </View>
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => setModalVisible(false)}
          >
            <Text style={styles.OkBtn}>OK </Text>
          </TouchableOpacity>
        </View>
      </Modal>

      {/* Auto Generated Number */}

      <View style={styles.innerView}>
        <View style={styles.borderedInput}>
          <TextInput style={styles.textInputWithoutBorder} placeholder="0" />
        </View>
        <Text style={[GlobalStyles.placeholder, { color: Colors.gray }]}>
          Amount
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  borderedInput: {
    height: 50,
    borderColor: Colors.darkGray,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  borderedInputView: {
    borderColor: Colors.gray,
    borderWidth: 1,
    height: 50,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    flex: 0.5,
  },
  container: {
    margin: 10,
  },
  headerText: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
  },
  innerView: {},
  listHeadView: {
    marginTop: 20,
  },
  minusView: {
    backgroundColor: "tomato",
    height: 20,
    width: 20,
    borderRadius: 10,
    position: "absolute",
    right: 0,
    zIndex: 100,
  },
  modalView: {
    marginVertical: 30,
    backgroundColor: "white",
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    flex: 1,
    shadowOpacity: 0.55,
    shadowRadius: 4,
    elevation: 5,
  },
  listText: {
    color: Colors.blue2,
    marginVertical: 25,
    fontWeight: "bold",
    marginHorizontal: 10,
    fontSize: 17,
  },
  OkBtn: {
    fontWeight: "800",
    fontSize: 17,
    alignSelf: "flex-end",
    color: Colors.main,
  },
  percentageVal: {
    fontSize: 18,
    textAlignVertical: "center",
    marginHorizontal: 10,
  },
  percentageView: {
    marginLeft: 10,
    width: 100,
  },
  percentageInnerView: {
    flexDirection: "row",
    flex: 1,
  },
  percentagePHolder: {
    textAlignVertical: "center",
    fontSize: 16,
  },
  searchView: {
    marginTop: 10,
    marginBottom: 15,
  },
  searchInput: {
    fontSize: 16,
    flex: 1,
  },
  searchLine: {
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    marginTop: 5,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 10,
    borderColor: Colors.black,
    borderWidth: 1,
    height: 50,
    borderRadius: 6,
    marginTop: 15,
  },
  textInputWithoutBorder: {
    fontSize: 16,
    paddingHorizontal: 10,
  },
});

export default AddProductDetail;

import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
  TouchableWithoutFeedback,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { MaterialCommunityIcons, Ionicons } from "@expo/vector-icons";
import CardView from "../SharedViews/CardView";
import { GlobalStyles } from "../Shared/GlobalStyles";
import FlatButton from "../SharedViews/Button";
import CaretDown from "../SharedViews/CaretDown";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Picker } from "@react-native-picker/picker";

function AddCustomer() {
  const [selectedLanguage, setSelectedLanguage] = useState(null);

  const [date, setDate] = useState(new Date());
  const [showDate, setShowDate] = useState(false);

  const [dueDate, setDueDate] = useState(new Date());
  const [showDueDate, setShowDueDate] = useState(false);

  const onDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowDate(Platform.OS === "ios");
    setDate(currentDate);
  };

  const onDueDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowDueDate(Platform.OS === "ios");
    setDueDate(currentDate);
  };

  const formatDate = () => {
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  };

  const formatDueDate = () => {
    return `${dueDate.getDate()}/${
      dueDate.getMonth() + 1
    }/${dueDate.getFullYear()}`;
  };

  return (
    <ScrollView>
      <SafeAreaView>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.primary} />

          {/* Personal Information */}

          <View style={styles.headerView}>
            <MaterialCommunityIcons name="account" style={styles.headerIcon} />
            <Text style={styles.headerTitle}>Personal Infotmation</Text>
            <Ionicons
              name="md-chevron-down-outline"
              style={styles.headerChevron}
            />
          </View>

          <View style={styles.headerLine} />

          <View style={GlobalStyles.flexDirRow}>
            {/*Series */}

            <View style={[styles.innerView, { flex: 0.5 }]}>
              <TouchableWithoutFeedback>
                <View>
                  <View style={styles.borderedInput}>
                    <Picker
                      selectedValue={selectedLanguage}
                      onValueChange={(itemValue, itemIndex) =>
                        setSelectedLanguage(itemValue)
                      }
                    >
                      <Picker.Item label="Java" value="java" />
                      <Picker.Item label="JavaScript" value="js" />
                    </Picker>

                    {/* <CaretDown/> */}
                  </View>
                  <Text style={styles.placeholder}>Series</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            {/* Customer Code */}

            <View style={[styles.innerView, { marginLeft: 10 }]}>
              <View style={styles.borderedInput}>
                <TextInput style={styles.textInput} placeholder="SAQ12000" />
              </View>
              <Text style={styles.placeholder}>Customer Code </Text>
            </View>
          </View>

          {/* Customer Name */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insert Customer Name"
              />
            </View>
            <Text style={styles.placeholder}> Customer Name</Text>
          </View>

          {/* Customer Category */}

          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Java" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>Customer Category</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Print Name */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insert Print Name"
              />
            </View>
            <Text style={styles.placeholder}>Print Name</Text>
          </View>

          {/* Display Name */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insert Display Name"
              />
            </View>
            <Text style={styles.placeholder}>Display Name</Text>
          </View>

          <View style={{ marginTop: 15 }} />

          {/* Balance Information */}

          <View style={styles.headerView}>
            <Ionicons name="md-calculator-outline" style={styles.headerIcon} />
            <Text style={styles.headerTitle}>Balance Infotmation</Text>
            <Ionicons
              name="md-chevron-down-outline"
              style={styles.headerChevron}
            />
          </View>

          <View style={styles.headerLine} />

          {/* Currency */}

          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Java" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>Currency</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <Text style={{ color: Colors.darkGray, marginLeft: 20 }}>
            (Can not changed once customer created)
          </Text>

          {/* Opening PKR */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="0" />
            </View>
            <Text style={styles.placeholder}> Opening PKR</Text>
          </View>

          {/* As of Date */}

          <View>
            <TouchableWithoutFeedback onPress={() => setShowDate(true)}>
              <View style={[styles.borderedInput, { flexDirection: "row" }]}>
                <Text style={styles.date}>{formatDate()}</Text>

                {showDate && (
                  <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode="date"
                    display="calendar"
                    onChange={onDateChange}
                  />
                )}

                <CaretDown />
              </View>
            </TouchableWithoutFeedback>

            <Text style={styles.placeholder}>As of Date</Text>
          </View>

          {/* Credit Limit Amount */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                keyboardType="numeric"
                placeholder="0.00"
              />
            </View>
            <Text style={styles.placeholder}>Credit Limit Amount</Text>
          </View>

          {/* Credit Limit Day */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                keyboardType="numeric"
                placeholder="0"
              />
            </View>
            <Text style={styles.placeholder}>Credit Limit Day</Text>
          </View>

          {/* Discount Percent */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                keyboardType="numeric"
                placeholder="Discount Percent"
              />
            </View>
            <Text style={styles.placeholder}>Discount Percent</Text>
          </View>

          <View style={{ marginTop: 15 }} />

          {/* Address Information */}

          <View style={styles.headerView}>
            <MaterialCommunityIcons
              name="map-marker-radius"
              style={styles.headerIcon}
            />
            <Text style={styles.headerTitle}>Address Infotmation</Text>
            <Ionicons
              name="md-chevron-down-outline"
              style={styles.headerChevron}
            />
          </View>

          <View style={styles.headerLine} />

          {/* Address Line 1 */}

          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Address Line 1"
              />
            </View>
            <Text style={styles.placeholder}>Address Line 1</Text>
          </View>

          {/* Address Line 2 */}

          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Address Line 2"
              />
            </View>
            <Text style={styles.placeholder}>Address Line 2</Text>
          </View>

          {/* City */}

          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="City" />
            </View>
            <Text style={styles.placeholder}>City</Text>
          </View>

          <View style={GlobalStyles.flexDirRow}>
            {/*State */}

            <View style={[styles.innerView, { flex: 0.5 }]}>
              <TouchableWithoutFeedback>
                <View>
                  <View style={styles.borderedInput}>
                    <TextInput style={styles.textInput} placeholder="State" />
                  </View>
                  <Text style={styles.placeholder}>State</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            {/* Zip */}

            <View style={[styles.innerView, { marginLeft: 10, flex: 0.5 }]}>
              <View style={styles.borderedInput}>
                <TextInput style={styles.textInput} placeholder="Zip" />
              </View>
              <Text style={styles.placeholder}>Zip </Text>
            </View>
          </View>

          {/* Country */}

          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Java" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>Country</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Email */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insertEmail"
                keyboardType="email-address"
              />
            </View>
            <Text style={styles.placeholder}> Email</Text>
          </View>

          {/* Contact Person */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insert Contact Person"
              />
            </View>
            <Text style={styles.placeholder}>Contact Person</Text>
          </View>

          {/* Phone */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insert Phone"
                keyboardType="phone-pad"
              />
            </View>
            <Text style={styles.placeholder}>Phone</Text>
          </View>

          {/* Fax */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insert Fax"
                keyboardType="phone-pad"
              />
            </View>
            <Text style={styles.placeholder}>Fax</Text>
          </View>

          <View style={GlobalStyles.flexDirRow}>
            {/* NTN */}

            <View style={[styles.innerView, { flex: 0.5 }]}>
              <TouchableWithoutFeedback>
                <View>
                  <View style={styles.borderedInput}>
                    <TextInput style={styles.textInput} placeholder="NTN" />
                  </View>
                  <Text style={styles.placeholder}>NTN</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            {/* CNIC */}

            <View style={[styles.innerView, { marginLeft: 10, flex: 0.5 }]}>
              <View style={styles.borderedInput}>
                <TextInput style={styles.textInput} placeholder="CNIC" />
              </View>
              <Text style={styles.placeholder}>CNIC </Text>
            </View>
          </View>

          {/* Main View */}
        </View>
      </SafeAreaView>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  amount: {
    textAlignVertical: "center",
    fontSize: 12,
    fontWeight: "bold",
  },
  borderedInput: {
    height: 50,
    borderColor: Colors.black,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  bottomContent: {
    textAlign: "center",
    fontSize: 13,
  },
  cardInnerView: {
    paddingVertical: 5,
    paddingLeft: 10,
  },
  cardView: {
    borderColor: Colors.separator,
    borderWidth: 1,
    alignSelf: "flex-end",
    backgroundColor: Colors.white,
    padding: 10,
    width: 300,
    marginTop: 15,
  },
  container: {
    margin: 10,
  },
  date: {
    fontSize: 15,
    textAlignVertical: "center",
    flex: 1,
    marginHorizontal: 10,
  },
  detailSubView: {},
  grayedText: {
    color: Colors.gray,
    fontSize: 15,
    textAlign: "right",
    marginRight: 5,
  },
  headerChevron: {
    color: Colors.gray,
    fontSize: 25,
    marginTop: 5,
  },
  headerIcon: {
    color: Colors.darkGray,
    fontSize: 27,
  },
  headerView: {
    alignItems: "center",
    flexDirection: "row",
    height: 40,
  },
  headerTitle: {
    color: Colors.blackBlue,
    marginLeft: 10,
    fontSize: 17,
    flex: 1,
  },
  headerLine: {
    height: 1,
    backgroundColor: Colors.gray,
    marginTop: 10,
    marginBottom: 15,
    marginHorizontal: -10,
  },
  innerView: {
    flex: 1,
  },
  leftContent: {
    color: Colors.textGreen,
    fontSize: 15,
    flex: 1,
  },
  percentTxt: {
    backgroundColor: Colors.gray,
    width: 30,
    textAlign: "center",
    fontSize: 18,
    textAlignVertical: "center",
    marginLeft: 7,
  },
  placeholder: {
    position: "absolute",
    backgroundColor: "white",
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  rowView: {
    flexDirection: "row",
    marginVertical: 15,
  },
  rightContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
  },
  rightBorderedContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    height: 30,
  },
  singleView: {},
  searchView: {
    marginTop: 10,
  },
  searchInput: {
    fontSize: 16,
    flex: 1,
  },
  searchLine: {
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    marginTop: 5,
  },
  separator: {
    backgroundColor: Colors.separator,
    width: 1,
    marginHorizontal: 7,
  },
  separatorBlack: {
    backgroundColor: Colors.black,
    height: 1,
    marginTop: 5,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 7,
  },
  topContent: {
    color: Colors.textGreen,
    fontSize: 13,
    fontWeight: "bold",
    marginRight: 10,
    textAlign: "center",
    marginBottom: 3,
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 0.96,
  },
});

export default AddCustomer;

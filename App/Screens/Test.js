import React, { Component } from "react";
import { View, Text, FlatList, TextInput, ListItem } from "react-native";
import { Button } from "react-native-paper";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";

function MyComponent() {
  const cameraLaunchOptions = {
    mediaType: "photo",
    quality: 0.7,
    cameraType: "front",
  };

  const captureImage = () => {
    launchCamera(cameraLaunchOptions, (data) => {
      console.log(data);
    });
  };

  return (
    <View>
      <Button onPress={captureImage}> Capture </Button>

      <Button> Pick </Button>
    </View>
  );
}

export default MyComponent;

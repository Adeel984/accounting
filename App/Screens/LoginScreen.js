import React, { useState } from "react";
import { View, StyleSheet, Text, TextInput, Pressable } from "react-native";
import { Colors } from "../Shared/Colors";
import { Ionicons } from "@expo/vector-icons";
import FlatButton from "../SharedViews/Button";
import { GlobalStyles } from "../Shared/GlobalStyles";
import { StoreData } from "../Shared/General";

function LoginScreen({ navigation }) {
  const [isPasswordShow, setIsPasswordShow] = useState(false);

  var OnPasswordHideShow = (onLayoutRootView) => {
    setIsPasswordShow(!isPasswordShow);
  };
  const OnLoginPressed = () => {
    StoreData("login", { userName: "Test", Password: "123" });
    navigation.navigate("homeDrawer");
  };
  const eyesIcon = isPasswordShow ? "eye" : "eye-off-sharp";

  return (
    <View style={GlobalStyles.container}>
      <Text style={styles.helloText}>Hello! Sign in with your email</Text>

      <TextInput style={styles.textInput} placeholder="Email" />

      <View style={styles.passView}>
        <TextInput
          style={styles.passInput}
          placeholder="Password"
          secureTextEntry={!isPasswordShow}
        />
        <Pressable onPress={OnPasswordHideShow} style={{ alignSelf: "center" }}>
          <Ionicons name={eyesIcon} size={24} color={Colors.black} />
        </Pressable>
      </View>

      <FlatButton text="Sign in" onPress={OnLoginPressed} />

      <Text style={styles.OrText}>OR</Text>

      <FlatButton text="Create new Account" backgroundColor={Colors.primary} />

      <View>
        <Text style={[styles.OrText, { fontWeight: "bold" }]}>
          Forgot Password?
        </Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  helloText: {
    textAlign: "center",
    fontWeight: "400",
    color: Colors.darkGray,
    fontSize: 15,
    marginBottom: 50,
  },
  OrText: {
    textAlign: "center",
    marginVertical: 10,
  },
  passView: {
    borderColor: Colors.black,
    borderWidth: 1,
    flexDirection: "row",
    height: 60,
    marginVertical: 15,
    borderRadius: 7,
  },
  passInput: {
    paddingLeft: 7,
    borderRadius: 5,
    fontSize: 17,
    marginVertical: 10,
    flex: 0.95,
  },
  textInput: {
    borderColor: Colors.black,
    borderWidth: 1,
    paddingLeft: 7,
    height: 60,
    borderRadius: 5,
    fontSize: 17,
  },
});

export default LoginScreen;

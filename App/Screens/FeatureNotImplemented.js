import React from "react";
import { View, Text, StyleSheet } from "react-native";

function FeatureNotImplemented(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        {" "}
        This feature is not available for mobile yet
      </Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  text: {
    fontWeight: "bold",
  },
});

export default FeatureNotImplemented;

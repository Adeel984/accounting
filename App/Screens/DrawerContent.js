import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableWithoutFeedback,
  SafeAreaView,
} from "react-native";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import { MaterialCommunityIcons, AntDesign } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Colors } from "../Shared/Colors";
import { GlobalStyles } from "../Shared/GlobalStyles";
import { StoreData } from "../Shared/General";
// import { FadeInOut } from "../SharedViews/FadeInOut";
function DrawerContent(props) {
  const navigation = props.navigation;

  const [selectedId, setSelectedId] = useState(null);
  const [DATA, setData] = useState([
    {
      id: "1",
      title: "Dashboard",
      icon: "home",
      nav: "home",
      collapsable: false,
      nestedList: [],
      collapsed: false,
    },
    {
      id: "2",
      title: "Sales",
      icon: "graph-outline",
      collapsable: true,
      nestedList: [
        {
          id: 21,
          title: "Quotation",
          nav: "quotationStack",
        },
        {
          id: 22,
          title: "Order",
          nav: "orderStack",
        },
        {
          id: 23,
          title: "Delivery",
          nav: "deliveryStack",
        },
        {
          id: 24,
          title: "Invoice",
          nav: "invoiceStack",
        },
        {
          id: 25,
          title: "Recurring Invoice",
          nav: "recurringInvoiceStack",
        },
        {
          id: 26,
          title: "Return",
          nav: "returnStack",
        },
        {
          id: 27,
          title: "Receive Money",
          nav: "receiveMoneyStack",
        },
        {
          id: 28,
          title: "Refund",
          nav: "refundStack",
        },
        {
          id: 29,
          title: "Settlement",
          nav: "settlementStack",
        },
      ],
      collapsed: false,
    },
    {
      id: "3",
      title: "Purchases",
      icon: "purse-outline",
      nav: "home",
      collapsable: true,
      nestedList: [
        {
          id: 31,
          title: "Order",
          nav: "purchaseOrderStack",
        },
        {
          id: 32,
          title: "Good Receiving",
          nav: "goodsRecevingStack",
        },
        {
          id: 33,
          title: "Invoice",
          nav: "purchaseInvoiceStack",
        },
        {
          id: 34,
          title: "Return",
          nav: "purchaseReturnStack",
        },
        {
          id: 35,
          title: "Make Payment",
          nav: "makePaymentStack",
        },
        {
          id: 36,
          title: "Refund",
          nav: "paymentRefundStack",
        },
        {
          id: 37,
          title: "Settlement",
          nav: "paymentSettlementStack",
        },
      ],
      collapsed: false,
    },
    {
      id: "4",
      title: "Accounts",
      icon: "calculator",
      nav: "home",
      collapsable: true,
      nestedList: [
        {
          id: 41,
          title: "Expense",
          nav: "accountsExpenseStack",
        },
        {
          id: 42,
          title: "Journal Entry",
          nav: "journalEntryStack",
        },
        {
          id: 43,
          title: "Chart of Account",
          nav: "chartOfAccountStack",
        },
        {
          id: 44,
          title: "Bank Account",
          nav: "bankAccountStack",
        },
        {
          id: 45,
          title: "Bank Deposit",
          nav: "bankDepositStack",
        },
        {
          id: 46,
          title: "Credit Note",
          nav: "creditNoteStack",
        },
        {
          id: 47,
          title: "Debit Note",
          nav: "debitNoteStack",
        },
        {
          id: 48,
          title: "Funds Transfer",
          nav: "fundsTransferStack",
        },
      ],
      collapsed: false,
    },
    {
      id: "5",
      title: "Inventory",
      icon: "share-outline",
      nav: "home",
      collapsable: true,
      nestedList: [
        {
          id: 51,
          title: "Stock Adjustment",
          nav: "stockAdjustmentsStack",
        },
        {
          id: 52,
          title: "Transfer Out",
          nav: "transferOutStack",
        },
        {
          id: 53,
          title: "Transfer In",
          nav: "transferInStack",
        },
      ],
      collapsed: false,
    },
    {
      id: "6",
      title: "Setup",
      icon: "cog-outline",
      nav: "home",
      collapsable: true,
      nestedList: [
        {
          id: 61,
          title: "Customers",
          nav: "customersStack",
        },
        {
          id: 62,
          title: "Vendors",
          nav: "vendorStack",
        },
        {
          id: 63,
          title: "Products",
          nav: "productStack",
        },
        {
          id: 64,
          title: "Taxes",
          nav: "taxesStack",
        },
        {
          id: 65,
          title: "Warehouse",
          nav: "warehouseStack",
        },
        {
          id: 66,
          title: "Employees",
          nav: "home",
        },
        {
          id: 67,
          title: "Brands",
          nav: "home",
        },
        {
          id: 68,
          title: "Departments",
          nav: "home",
        },
        {
          id: 69,
          title: "Designations",
          nav: "home",
        },
        {
          id: 70,
          title: "Adjustment Types",
          nav: "adjustmentTypesStack",
        },
        {
          id: 71,
          title: "Custom Fields",
          nav: "home",
        },
        {
          id: 72,
          title: "Customer Categories",
          nav: "customCategoriesStack",
        },
        {
          id: 73,
          title: "Vendor Categories",
          nav: "home",
        },
        {
          id: 74,
          title: "Product Categories",
          nav: "productCategoriesStack",
        },
      ],
      collapsed: false,
    },
    {
      id: "7",
      title: "Sync Now",
      icon: "sync",
      nav: "Sync",
      collapsable: false,
      nestedList: [],
      collapsed: false,
    },
    {
      id: "8",
      title: "Reports",
      icon: "file",
      nav: "Reports",
      collapsable: false,
      nestedList: [],
      collapsed: false,
    },
    {
      id: "9",
      title: "Get Free Credit",
      icon: "cash",
      nav: "home",
      collapsable: false,
      nestedList: [],
      collapsed: false,
    },
    {
      id: "10",
      title: "Logout",
      icon: "logout",
      nav: "home",
      collapsable: false,
      nestedList: [],
      collapsed: false,
    },
  ]);

  const onDrawerSelected = (item) => {
    // console.log("onDrawerSelected" + item.title);
    if (item.title === "Logout") {
      StoreData("login", null);
      navigation.navigate("login");
      navigation.closeDrawer();
    } else if (item.collapsable) {
      //  item.collapsed = false;
      setSelectedId(item.id);
    } else {
      console.log(item.nav);
      navigation.navigate(item.nav);
      // switch (item.title) {
      //   case "Dashboard":
      //     navigation.navigate("Home");
      //     break;
      //   case "Quotation":
      //     navigation.navigate("Sale Quotations");
      //     break;
      //   case "Order":
      //     navigation.navigate("Sale Orders");
      //     break;
      //   case "Delivery":
      //     navigation.navigate("Sale Delivery");
      //     break;
      //   case "Invoice":
      //     navigation.navigate("Invoice");
      //     break;
      //   case "Recurring Invoice":
      //     navigation.navigate("Recurring Invoice");
      //     break;
      //   case "Return":
      //     navigation.navigate("ReceiveMoney");
      //     break;
      //   case "Invoice":
      //     navigation.navigate("Refund");
      //     break;
      // }
    }
  };

  const Item = ({ item, onPress }) => {
    if (selectedId === item.id) {
      item.collapsed = !item.collapsed;
      setSelectedId(null);
    }

    var menuIc = !item.collapsed ? "menu-right" : "menu-down";
    return (
      <View style={{ flexGrow: 1 }}>
        <TouchableWithoutFeedback style={{ flex: 1 }} onPress={onPress}>
          <View style={styles.item}>
            <MaterialCommunityIcons name={item.icon} style={styles.icon} />
            <Text style={styles.title}>{item.title}</Text>
            {item.collapsable && (
              <MaterialCommunityIcons name={menuIc} style={styles.subMenuIc} />
            )}
          </View>
        </TouchableWithoutFeedback>
        {item.collapsed && (
          <View>
            {item.nestedList.map((nestedItem, index) => (
              <TouchableWithoutFeedback
                key={nestedItem.id}
                style={styles.container}
                onPress={() => onDrawerSelected(nestedItem)}
              >
                <Text style={styles.nestedTitle}>{nestedItem.title}</Text>
              </TouchableWithoutFeedback>
            ))}
          </View>
        )}

        {/* <View style={{ height: 1, backgroundColor: "#e1e1e1" }} /> */}
      </View>
    );
  };

  const renderItem = ({ item }) => {
    return <Item item={item} onPress={() => onDrawerSelected(item)} />;
  };

  return (
    <SafeAreaView style={GlobalStyles.flex1}>
      <View style={styles.main}>
        <View style={styles.headerNav}>
          <TouchableOpacity onPress={() => navigation.closeDrawer()}>
            <AntDesign name="arrowleft" size={26} color="black" />
          </TouchableOpacity>
        </View>
        <View style={styles.header}>
          <View style={styles.image} />

          <Image
            height={40}
            width={40}
            source={{
              uri: "https://lh3.googleusercontent.com/proxy/J_nhYhG3mHPLuNemFSqOEDMPteRSnkKNk9xXHr561HKCg3CUztXznlWO_Ce_nNnIiHk9qkF2l48Y7gxYEedCSCvUwVo5Sx4tXNBHfbV7qXgRKzNdZ32poMMH4I_W",
            }}
          />

          <View style={{ justifyContent: "center" }}>
            <Text style={styles.userTitle}>Jaweed Trading</Text>
            <Text style={styles.email}>it.exp3rt@gmail.com</Text>
          </View>
        </View>

        <FlatList
          data={DATA}
          style={{ marginTop: -10 }}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          extraData={selectedId}
          ListFooterComponent={<View style={{ height: 180 }} />}
        />

        {/* <View
        style={{
          height: 30,
          flex: 0.07,
          padding: 10,
          backgroundColor: "#e1e1e1",
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <MaterialCommunityIcons
            name="logout-variant"
            size={30}
            color="gray"
            style={{ marginRight: 10 }}
          />
          <Text style={styles.title}>Logout</Text>
        </View>
      </View> */}
      </View>
    </SafeAreaView>
  );
}

export default DrawerContent;

const styles = StyleSheet.create({
  email: {
    color: Colors.white,
    fontSize: 12,
  },
  header: {
    backgroundColor: "tomato",
    padding: 10,
    flexDirection: "row",
    height: 120,
    alignItems: "center",
  },
  headerNav: {
    backgroundColor: Colors.gray,
    height: 45,
    paddingHorizontal: 10,
    justifyContent: "center",
  },
  icon: {
    color: Colors.black,
    fontSize: 26,
    marginRight: 10,
  },
  image: {
    height: 70,
    width: 70,
    backgroundColor: "white",
    borderColor: "black",
    borderWidth: 1,
    borderRadius: 35,
    marginRight: 8,
  },
  item: {
    flexDirection: "row",
    flex: 1,
    marginTop: 20,
    marginHorizontal: 15,
  },
  nestedTitle: {
    marginLeft: 60,
    marginTop: 15,
    fontSize: 17,
    marginBottom: 5,
  },
  nestedView: {
    height: 20,
  },
  subMenuIc: {
    color: Colors.black,
    fontSize: 32,
    position: "absolute",
    right: -10,
    textAlignVertical: "center",
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: Colors.darkGray,
    textAlignVertical: "bottom",
  },
  userTitle: {
    fontSize: 17,
    color: "white",
    fontWeight: "600",
  },
});

import React, { useState, createRef, useRef, useCallback } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Modal,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import CardView from "../SharedViews/CardView";
import { GlobalStyles } from "../Shared/GlobalStyles";
import FlatButton from "../SharedViews/Button";
import CaretDown from "../SharedViews/CaretDown";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Picker } from "@react-native-picker/picker";
import DropDownPicker from "react-native-dropdown-picker";
import { DatePickerModal } from "react-native-paper-dates";
import Menu, { MenuItem, MenuDivider } from "react-native-material-menu";
import { Button, IconButton } from "react-native-paper";
import ActionSheetView from "../SharedViews/ActionSheet";
import { Formik } from "formik";
import * as yup from "yup";
import SearchableDropdown from "react-native-searchable-dropdown";

// import "intl";
// import "intl/locale-data/jsonp/en";

const validationSchema = yup.object({
  // email: yup.string().required().email().min(3),
  // age: yup
  //   .string()
  //   .required()
  //   .test("is-age-18-or-above", "Age must be above 18", (val) => {
  //     return parseInt(val) > 18;
  //   }),
  series: yup.string().required(),
  reference: yup.string().required(),
  customer: yup.string().required(),
});

function AddSaleQuotation({ navigation }) {
  const formikRef = createRef();
  const [visible, setVisible] = useState(false);
  const [customer, setCustomer] = useState("");
  const [customerSearch, setCustomerSearch] = useState("");
  const searchRef = useRef();

  const openMenu = () => setVisible(true);

  const closeMenu = () => setVisible(false);

  const [date, setDate] = useState(new Date());
  const [showDate, setShowDate] = useState(false);

  const onDismissSingle = useCallback(() => {
    setShowDate(false);
  }, [setShowDate]);

  const onConfirmSingle = useCallback(
    (params) => {
      setShowDate(false);
      setDate(params.date);
    },
    [setShowDate, setDate]
  );

  const [expiryDate, setExpiryDate] = useState(new Date());
  const [showExpiryDate, setShowExpiryDate] = useState(false);
  const [series, setSeries] = useState("Select Series");
  const [searchFocused, setSearchFocused] = useState("false");

  const onDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowDate(Platform.OS === "ios");
    setDate(currentDate);
  };

  const onExpiryDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowExpiryDate(Platform.OS === "ios");
    setExpiryDate(currentDate);
  };

  const formatDate = () => {
    let fmDate = `${date.getDate()}/${
      date.getMonth() + 1
    }/${date.getFullYear()}`;
    // formikRef?.current?.setFieldValue("date", fmDate ?? "");
    return fmDate;
  };

  const formatExpiryDate = () => {
    return `${expiryDate.getDate()}/${
      expiryDate.getMonth() + 1
    }/${expiryDate.getFullYear()}`;
  };

  const [menuRef, setMenuRef] = useState();

  const [customerListRef, setCustomerListRef] = useState();

  const [actionRef, setActionRef] = useState();
  const [customerList, setCustomerList] = useState([]);

  const hideActions = (item) => {
    actionRef.hide();
    formikRef.current.setFieldValue("date", formatDate());
    formikRef?.current.handleSubmit();
  };

  const showActions = () => {
    actionRef?.show();
  };

  const hideMenu = (menu) => {
    menuRef.hide();
    console.log(menu);
    setSeries(menu.title);
  };

  const showMenu = () => {
    menuRef.show();
  };

  const customers = [
    { name: "raza", id: 123 },
    { name: "Kam", id: 124 },
    { name: "sham", id: 125 },
    { name: "sam", id: 126 },
  ];

  const seriesList = [
    {
      id: "123",
      title: "SQ",
    },
  ];

  const actionList = [
    {
      id: "1",
      title: "Save and Close",
    },
    {
      id: "1",
      title: "Save and New",
    },
    {
      id: "1",
      title: "Save and Continue Edit",
    },
    {
      id: "1",
      title: "Save and Approve",
    },
  ];

  const actionItems = [
    {
      id: 1,
      label: "Camera",
      onPress: () => {
        console.log("Camera");
      },
    },
    {
      id: 2,
      label: "Gallery",
      onPress: () => {
        console.log("Gallery");
      },
    },
  ];
  const [actionSheet, setActionSheet] = useState(false);
  const closeActionSheet = () => {
    console.log("closing action Sheet", actionSheet);
    setActionSheet(!actionSheet);
  };

  // React.useLayoutEffect(() => {
  navigation.setOptions({
    headerRight: () => (
      <Menu
        ref={setActionRef}
        button={
          <IconButton
            icon="dots-vertical"
            size={30}
            style={{ marginLeft: -2 }}
            onPress={() => showActions()}
            color={Colors.white}
          />
        }
      >
        {actionList.map((item, index) => (
          <MenuItem onPress={() => hideActions(item)}>{item.title}</MenuItem>
        ))}
      </Menu>
    ),
  });

  const searchCustomer = (text) => {
    const newData = customers.filter((item) => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    setCustomerList(newData);
    //  setCustomer(text);
  };
  // }, [navigation]);

  return (
    <ScrollView style={GlobalStyles.flex1}>
      <SafeAreaView style={GlobalStyles.flex1}>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.primary} />

          {/* Search View */}
          <View style={styles.searchView}>
            <View style={[GlobalStyles.flexDirRow, { alignItems: "center" }]}>
              <TextInput
                style={styles.searchInput}
                placeholder="Customer"
                value={customerSearch}
                blurOnSubmit={false}
                ref={searchRef}
                returnKeyType="next"
                onChangeText={async (val) => {
                  setCustomerSearch(val);
                  // setCustomerList(
                  //   customers.filter((p) => p.name.includes(val))
                  // );
                  searchCustomer(val);

                  //customerList = customers.filter((p) => p.name.includes(val));
                  console.log("customerList", customerList);
                  // customerListRef.show();
                  // await new Promise(() =>
                  //   setTimeout(() => {
                  //     searchRef.current.focus();
                  //   }, 1000)
                  // );
                }}
              />
              <MaterialIcons
                name="cancel"
                onPress={() => setCustomer("")}
                size={22}
                color={Colors.darkGray}
              />
            </View>
            <View style={styles.searchLine} />
          </View>

          {/* <Menu
            ref={setCustomerListRef}
            style={{ backgroundColor: Colors.white }}
          >
            {customerList.map((item, index) => (
              <MenuItem
                onPress={() => {
                  customerListRef.hide();
                  console.log(item);
                  setCustomer(item.name);
                }}
              >
                {item.name}
              </MenuItem>
            ))}
          </Menu> */}

          <Formik
            innerRef={formikRef}
            initialValues={{ series: "", reference: "", date: "" }}
            validationSchema={validationSchema}
            onSubmit={(values, actions) => {
              console.log(values);
              actions.resetForm();
            }}
          >
            {({ handleChange, handleBlur, values, errors, setFieldValue }) => (
              <View>
                <View style={GlobalStyles.flexDirRow}>
                  {/*Series */}

                  <View style={styles.innerView}>
                    {/* Date */}

                    <TouchableWithoutFeedback onPress={showMenu}>
                      <View
                        style={[
                          styles.borderedInput,
                          {
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "flex-start",
                          },
                        ]}
                      >
                        <View style={GlobalStyles.flex1}>
                          <Menu
                            ref={setMenuRef}
                            button={
                              <Text style={{ marginLeft: 5 }}>{series}</Text>
                            }
                          >
                            {seriesList.map((item, index) => (
                              <MenuItem
                                onPress={() => {
                                  hideMenu(item);
                                  setFieldValue("series", item.title);
                                }}
                              >
                                {item.title}
                              </MenuItem>
                            ))}
                          </Menu>
                        </View>
                        <CaretDown />
                      </View>
                    </TouchableWithoutFeedback>

                    <Text style={styles.placeholder}>Series</Text>
                  </View>

                  {/* Auto Generated Number */}

                  <View style={[styles.innerView, { marginLeft: 10 }]}>
                    <View style={styles.borderedInput}>
                      <TextInput
                        style={styles.textInput}
                        placeholder="SAQ12000"
                      />
                    </View>
                    <Text style={styles.placeholder}>
                      Auto Generated Number
                    </Text>
                  </View>
                </View>

                {errors.reference && (
                  <Text style={styles.errorText}>{errors.series}</Text>
                )}

                {/* Line 2 */}

                <View style={GlobalStyles.flexDirRow}>
                  <View style={styles.innerView}>
                    {/* Date */}

                    <TouchableWithoutFeedback onPress={() => setShowDate(true)}>
                      <View
                        style={[
                          styles.borderedInput,
                          { flexDirection: "row", alignItems: "center" },
                        ]}
                      >
                        <Text style={styles.date}>{formatDate()}</Text>

                        {/* {showDate && (
                    <DateTimePicker
                      testID="dateTimePicker"
                      value={date}
                      mode="date"
                      display="calendar"
                      onChange={onDateChange}
                    />
                  )} */}
                        <DatePickerModal
                          // locale={'en'} optional, default: automatic
                          mode="single"
                          visible={showDate}
                          onDismiss={onDismissSingle}
                          date={date}
                          onConfirm={onConfirmSingle}
                          // validRange={{
                          //   startDate: new Date(2021, 1, 2),  // optional
                          //   endDate: new Date(), // optional
                          // }}
                          // onChange={} // same props as onConfirm but triggered without confirmed by user
                          // saveLabel="Save" // optional
                          // label="Select date" // optional
                          // animationType="slide" // optional, default is 'slide' on ios/android and 'none' on web
                        />

                        <CaretDown />
                      </View>
                    </TouchableWithoutFeedback>

                    <Text style={styles.placeholder}>Date</Text>
                  </View>

                  {/* Expiry Date */}

                  <View style={[styles.innerView, { marginLeft: 10 }]}>
                    <TouchableWithoutFeedback
                      onPress={() => setShowExpiryDate(true)}
                    >
                      <View
                        style={[
                          styles.borderedInput,
                          { flexDirection: "row", alignItems: "center" },
                        ]}
                      >
                        <Text style={styles.date}>{formatExpiryDate()}</Text>

                        {showExpiryDate && (
                          <DateTimePicker
                            value={expiryDate}
                            mode="date"
                            display="inline"
                            onChange={onExpiryDateChange}
                          />
                        )}

                        <CaretDown />
                      </View>
                    </TouchableWithoutFeedback>
                    <Text style={styles.placeholder}>Expiry Date</Text>
                  </View>
                </View>

                {/* Refference */}

                <View style={styles.singleView}>
                  <View style={styles.borderedInput}>
                    <TextInput
                      style={styles.textInput}
                      onChangeText={handleChange("reference")}
                      value={values.reference}
                      onBlur={handleBlur("reference")}
                      placeholder="Please insert refference"
                    />
                  </View>
                  <Text style={styles.placeholder}>Refference</Text>
                </View>

                <Text style={styles.errorText}>{errors.reference}</Text>

                {/* Currency */}

                <View style={styles.singleView}>
                  <View style={styles.borderedInput}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Please insert currency"
                    />
                  </View>
                  <Text style={styles.placeholder}>Currency</Text>
                </View>

                {/* Terms */}

                <View style={[styles.singleView, { marginBottom: 10 }]}>
                  <View style={[styles.borderedInput, { height: 70 }]}>
                    <TextInput
                      multiline
                      style={styles.textInput}
                      placeholder="Please insert terms"
                    />
                  </View>
                  <Text style={styles.placeholder}>Terms</Text>
                </View>

                <Button
                  icon="upload"
                  mode="contained"
                  color={Colors.main}
                  labelStyle={{ color: Colors.white }}
                  onPress={() => ActionSheetView.defaultProps.onShow()}
                  style={{ marginBottom: 10 }}
                >
                  Upload Attachment
                </Button>

                <Button
                  icon="playlist-edit"
                  mode="contained"
                  color={Colors.primary}
                  labelStyle={{ color: Colors.white }}
                  onPress={() => console.log("Pressed")}
                >
                  Add Product Detail
                </Button>
              </View>
            )}
          </Formik>

          <View
            style={[
              GlobalStyles.card,
              {
                position: "absolute",
                top: 0,
                flex: 1,
                marginTop: 60,
                width: 380,
              },
            ]}
          >
            {customerList.map((item, index) => (
              <View>
                <Text
                  style={{ margin: 10 }}
                  onPress={() => {
                    console.log(item);
                    setCustomer(item.name);
                    setCustomerSearch(item.name);
                    setCustomerList([]);
                  }}
                >
                  {item.name}
                </Text>
                <View style={GlobalStyles.separator} />
              </View>
            ))}
          </View>

          {/* <FlatButton text="Upload Attachment" /> */}

          {/* CardView */}
          <View style={{ marginTop: 20 }} />

          <CardView>
            <View style={styles.cardInnerView}>
              {/* First row */}

              <View style={GlobalStyles.flexDirRow}>
                <Text style={styles.userName}>Soda</Text>
                <Text style={styles.amount}>100.00</Text>
              </View>

              {/* 2nd row */}

              <View style={[GlobalStyles.flexDirRow, { marginTop: 10 }]}>
                <View style={styles.detailSubView}>
                  <Text style={styles.topContent}>Qty</Text>
                  <Text style={styles.bottomContent}>1.00 Kg</Text>
                </View>

                <View style={styles.separator} />

                <View style={styles.detailSubView}>
                  <Text style={styles.topContent}>Price</Text>
                  <Text style={styles.bottomContent}>2.00</Text>
                </View>

                <View style={styles.separator} />

                <View style={styles.detailSubView}>
                  <Text style={styles.topContent}>Description</Text>
                  <Text style={styles.bottomContent}>Lorem ipsum</Text>
                </View>

                <View style={styles.separator} />

                <View style={styles.detailSubView}>
                  <Text style={styles.topContent}>Tax Amount</Text>
                  <Text style={styles.bottomContent}>100l.00</Text>
                </View>
              </View>
            </View>
          </CardView>

          <View style={styles.cardView}>
            <View style={[styles.rowView, { marginTop: 0 }]}>
              <Text style={styles.leftContent}>Gross:</Text>

              <Text style={styles.rightContent}>100.00</Text>
            </View>

            <Text style={styles.leftContent}>Discount:</Text>

            <View style={GlobalStyles.flexDirRow}>
              <View style={[GlobalStyles.flex1, { marginLeft: 75 }]}>
                <Text style={styles.grayedText}>5.0</Text>
                <View style={styles.separatorBlack} />
              </View>

              <View style={[GlobalStyles.flex1, { marginLeft: 10 }]}>
                <Text style={styles.grayedText}>5.0</Text>
                <View style={styles.separatorBlack} />
              </View>
            </View>

            <View style={styles.rowView}>
              <Text style={styles.leftContent}>Tax:</Text>

              <Text style={styles.rightContent}>17.00</Text>
            </View>

            <View style={styles.rowView}>
              <Text
                style={[styles.leftContent, { fontWeight: "bold", flex: 0 }]}
              >
                {" "}
                Net PKR:
              </Text>

              <View style={GlobalStyles.flex1}>
                <Text style={[styles.rightContent, { fontWeight: "bold" }]}>
                  122.00
                </Text>

                <View style={styles.separatorBlack} />
              </View>
            </View>
          </View>
        </View>

        <View style={styles.backgroundView} />

        {/* <ActionSheet ref={actionSheetRef}>
          <View>
            <Text>YOUR CUSTOM COMPONENT INSIDE THE ACTIONSHEET</Text>
          </View>
        </ActionSheet> */}
        <ActionSheetView
          actionItems={actionItems}
          onCancel={closeActionSheet}
        />

        {/* <Modal visible={actionSheet} animationType="slide" transparent>
         
        </Modal> */}
      </SafeAreaView>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  amount: {
    textAlignVertical: "center",
    fontSize: 12,
    fontWeight: "bold",
  },
  backgroundView: {
    backgroundColor: Colors.gray,
    flex: 1,
    opacity: 1,
  },
  borderedInput: {
    height: 50,
    borderColor: Colors.black,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  bottomContent: {
    textAlign: "center",
    fontSize: 13,
  },
  cardInnerView: {
    paddingVertical: 5,
    paddingLeft: 10,
  },
  cardView: {
    borderColor: Colors.separator,
    borderWidth: 1,
    alignSelf: "flex-end",
    backgroundColor: Colors.white,
    padding: 10,
    width: 280,
    marginTop: 15,
  },
  container: {
    margin: 10,
    backgroundColor: Colors.bg,
  },
  date: {
    fontSize: 15,
    textAlignVertical: "center",
    flex: 1,
    marginHorizontal: 10,
  },
  detailSubView: {},
  errorText: {
    color: Colors.red,
  },
  grayedText: {
    color: Colors.gray,
    fontSize: 15,
    textAlign: "right",
    marginRight: 5,
  },
  innerView: {
    flex: 1,
  },
  leftContent: {
    color: Colors.textGreen,
    fontSize: 15,
    flex: 1,
  },
  placeholder: {
    position: "absolute",
    backgroundColor: Colors.bg,
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  rowView: {
    flexDirection: "row",
    marginVertical: 15,
  },
  rightContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
  },
  singleView: {},
  searchView: {
    marginTop: 10,
  },
  searchInput: {
    fontSize: 16,
    flex: 1,
  },
  searchLine: {
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    marginTop: Platform.OS === "android" ? 0 : 10,
    marginBottom: Platform.OS === "android" ? 0 : 5,
  },
  separator: {
    backgroundColor: Colors.separator,
    width: 1,
    marginHorizontal: 7,
  },
  separatorBlack: {
    backgroundColor: Colors.black,
    height: 1,
    marginTop: 5,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 7,
  },
  topContent: {
    color: Colors.textGreen,
    fontSize: 13,
    fontWeight: "bold",
    marginRight: 10,
    textAlign: "center",
    marginBottom: 3,
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 0.96,
  },
});

export default AddSaleQuotation;

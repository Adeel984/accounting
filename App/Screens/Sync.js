import React from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import CardView from "../SharedViews/CardView";
import { Button } from "react-native-paper";
import { GlobalStyles } from "../Shared/GlobalStyles";

function Sync() {
  const DATA = [
    {
      id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
      title: "Country",
    },
    {
      id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28bc",
      title: "Customers",
    },
  ];

  const renderItem = ({ item }) => {
    return (
      <CardView>
        <View style={styles.rowDirection}>
          <View style={GlobalStyles.flex1}>
            <Text style={styles.boldFont}>{item.title}</Text>
            <Text style={styles.lastSync}>Last sync Date 31-May-2021</Text>
          </View>
          <Button
            style={styles.button}
            labelStyle={{
              textAlignVertical: "bottom",
              fontSize: 16,
            }}
            color={Colors.white}
            onPress={() => console.log("Pressed")}
          >
            {""}
            Sync
          </Button>
        </View>
      </CardView>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.primary} />

      <FlatList
        data={DATA}
        style={styles.listView}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  boldFont: {
    fontWeight: "bold",
    fontSize: 17,
    color: Colors.black,
  },
  button: {
    backgroundColor: Colors.green,
    borderRadius: 5,
  },
  container: {
    margin: 10,
  },
  lastSync: {
    color: Colors.darkGray,
    marginTop: 5,
    fontSize: 12,
  },
  listView: {
    backgroundColor: Colors.lightGray,
    marginTop: 2,
  },
  refNumber: {
    textAlignVertical: "center",
    fontSize: 11,
    marginHorizontal: 4,
  },
  rowDirection: {
    flexDirection: "row",
    padding: 5,
  },
});

export default Sync;

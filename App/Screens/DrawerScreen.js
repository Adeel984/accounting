import * as React from "react";
import { Button, Settings, View } from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import Dashboard from "./Dashboard";
import DrawerContent from "./DrawerContent";
import { Colors } from "../Shared/Colors";
import HeaderButton from "../SharedViews/HeaderButton";
import {
  QuotationStack,
  OrderStack,
  DeliveryStack,
  InvoiceStack,
  ReturnStack,
  ReceiveMoneyStack,
  CustomerRefundStack,
  RecurringStack,
  CustomerSettlementStack,
} from "../Routes/SalesRoutes";

import {
  purchaseOrdersStack,
  goodsRecevingStack,
  purchaseInvoiceStack,
  purchaseReturnStack,
  makePaymentStack,
  paymentRefundStack,
  paymentSettlementStack,
} from "../Routes/PurchasesRoutes";

import {
  accountsExpenseStack,
  journalEntryStack,
  chartOfAccountStack,
  bankAccountStack,
  bankDepositStack,
  creditNoteStack,
  debitNoteStack,
  fundsTransferStack,
} from "../Routes/AccountsRoots";

import {
  stockAdjustmentsStack,
  transferOutStack,
  transferInStack,
} from "../Routes/InventoryStack";

import {
  customersStack,
  vendorStack,
  productStack,
  taxesStack,
  warehouseStack,
  adjustmentTypesStack,
  customCategoriesStack,
  productCategoriesStack,
} from "../Routes/SetupRoutes";
import Sync from "./Sync";
import Reports from "./Reports";

const Drawer = createDrawerNavigator();
function DrawerScreen() {
  const onSaleQuotationAdd = (props) => {
    console.log(props);
  };
  return (
    <Drawer.Navigator
      initialRouteName="home"
      drawerContent={(props) => <DrawerContent {...props} />}
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.main,
        },
        headerShown: true,
        headerTintColor: Colors.white,
        headerTitleStyle: {
          //fontWeight: "bold",
        },
      }}
    >
      <Drawer.Screen
        options={{ title: "Dashboard" }}
        name="home"
        component={Dashboard}
      />
      <Drawer.Screen
        options={{ headerShown: false }}
        name="quotationStack"
        component={QuotationStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="orderStack"
        component={OrderStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="deliveryStack"
        component={DeliveryStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="invoiceStack"
        component={InvoiceStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="recurringInvoiceStack"
        component={RecurringStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="returnStack"
        component={ReturnStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="receiveMoneyStack"
        component={ReceiveMoneyStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="refundStack"
        component={CustomerRefundStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="settlementStack"
        component={CustomerSettlementStack}
      />

      {/* Purchases Stack */}

      <Drawer.Screen
        options={{ headerShown: false }}
        name="purchaseOrderStack"
        component={purchaseOrdersStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="goodsRecevingStack"
        component={goodsRecevingStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="purchaseInvoiceStack"
        component={purchaseInvoiceStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="purchaseReturnStack"
        component={purchaseReturnStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="makePaymentStack"
        component={makePaymentStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="paymentRefundStack"
        component={paymentRefundStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="paymentSettlementStack"
        component={paymentSettlementStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="accountsExpenseStack"
        component={accountsExpenseStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="journalEntryStack"
        component={journalEntryStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="chartOfAccountStack"
        component={chartOfAccountStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="bankAccountStack"
        component={bankAccountStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="bankDepositStack"
        component={bankDepositStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="creditNoteStack"
        component={creditNoteStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="debitNoteStack"
        component={debitNoteStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="fundsTransferStack"
        component={fundsTransferStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="stockAdjustmentsStack"
        component={stockAdjustmentsStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="transferOutStack"
        component={transferOutStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="transferInStack"
        component={transferInStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="customersStack"
        component={customersStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="vendorStack"
        component={vendorStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="productStack"
        component={productStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="taxesStack"
        component={taxesStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="warehouseStack"
        component={warehouseStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="adjustmentTypesStack"
        component={adjustmentTypesStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="customCategoriesStack"
        component={customCategoriesStack}
      />

      <Drawer.Screen
        options={{ headerShown: false }}
        name="productCategoriesStack"
        component={productCategoriesStack}
      />

      <Drawer.Screen name="Sync" component={Sync} />
      <Drawer.Screen name="Reports" component={Reports} />
    </Drawer.Navigator>
  );
}

export default DrawerScreen;

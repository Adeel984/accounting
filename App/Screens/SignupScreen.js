import React, {useState} from 'react';
import {View,StyleSheet, Text, TextInput, Pressable, TouchableOpacity} from 'react-native';
import {Colors } from '../Shared/Colors';
import {Ionicons} from '@expo/vector-icons';
import FlatButton from '../SharedViews/Button';
import { GlobalStyles } from '../Shared/GlobalStyles';

function SignupScreen() {

    const [isPasswordShow,setIsPasswordShow] = useState(false);

    var OnPasswordHideShow = () => {
        setIsPasswordShow(!isPasswordShow);
    }
    const eyesIcon = isPasswordShow ? 'eye' : 'eye-off-sharp';

    return (
        <View style={GlobalStyles.container}>

            <TextInput style={styles.textInput} placeholder='Full Name'/>

            <TextInput style={styles.textInput} placeholder='Email' keyboardType='email-address'/>

            <TextInput style={styles.textInput} placeholder='Phone' keyboardType='phone-pad'/>

            <View style={styles.passView}>
                <TextInput style={styles.passInput} placeholder='Password' secureTextEntry={!isPasswordShow}/>
                <Pressable onPress={OnPasswordHideShow} style={{alignSelf:'center'}}>
                    <Ionicons name={eyesIcon} size={24} color={Colors.black}/>
                </Pressable>
            </View>

            <Text style={styles.bottomText}>By clicking 'Sign Up' button you agree to our</Text>

            <TouchableOpacity>
                <Text style={styles.termsCondition}>Terms and Conditions</Text>
            </TouchableOpacity>

            <FlatButton text='Sign Up' backgroundColor={Colors.primary}/>

            <View style={styles.alreadyhaveanAccount}>
                <Text>Already have an account?</Text>
                
                <TouchableOpacity>
                    <Text style={ {fontWeight:'bold'}}> Sign in</Text>
                </TouchableOpacity>
            </View>
            
            
        </View>
    );
}
const styles = StyleSheet.create({
    alreadyhaveanAccount:{
        alignSelf:'center',
        flexDirection:'row',
        marginTop:10
    },
    bottomText:{
        textAlign:'center',
        marginTop:20,
        fontSize:16
    },
    helloText: {
        textAlign:'center',
        fontWeight:'400',
        color: Colors.darkGray,
        fontSize: 15,
        marginBottom:50,
    },
    passView : {
        borderColor: Colors.black,
        borderWidth:1,
        flexDirection:'row',
        height:60,
        borderRadius:7
    },
    passInput:{
        paddingLeft:7,
        borderRadius:5,
        fontSize:17,
        flex:0.95
    },
    termsCondition:{
        color:Colors.primary,
        fontWeight:'bold',
        marginBottom:10,
        textAlign:'center',
        fontSize:17,
        marginTop:5
    },
    textInput:{
        borderColor: Colors.black,
        borderWidth:1,
        paddingLeft:7,
        height:60,
        borderRadius:5,
        fontSize:17,
        marginBottom:15
    }
})

export default SignupScreen;
import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
  TouchableWithoutFeedback,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { MaterialIcons } from "@expo/vector-icons";
import { GlobalStyles } from "../Shared/GlobalStyles";
import CaretDown from "../SharedViews/CaretDown";
import { Picker } from "@react-native-picker/picker";
import { RadioButton, Checkbox, Button } from "react-native-paper";
import { TextInput as Input } from "react-native-paper";
import CardView from "../SharedViews/CardView";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

const Tab = createMaterialTopTabNavigator();

function AddProduct() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="General" component={GeneralDetail} />
      <Tab.Screen name="Detail" component={Detail} />
      <Tab.Screen name="Custom Fields" component={CustomFields} />
      <Tab.Screen name="Other Setting" component={OtherSetting} />
    </Tab.Navigator>
  );
}

function OtherSetting() {
  const [selectedCountry, setSelectedCountry] = useState();
  const [checkBoxchecked, setCheckBoxchecked] = React.useState(true);
  return (
    <ScrollView style={GlobalStyles.outSideContainer}>
      <View style={styles.container}>
        <StatusBar backgroundColor={Colors.primary} />

        {/* Do you purchase or sale in fractional units? */}
        <View style={styles.cbView}>
          <Checkbox
            status={checkBoxchecked ? "checked" : "unchecked"}
            onPress={() => {
              setCheckBoxchecked(!checkBoxchecked);
            }}
          />
          <View style={styles.cbRightView}>
            <Text style={styles.cbText}>
              Do you purchase or sale in fractional units?
            </Text>
            <Text style={styles.cbPlaceHolder}>
              (e.g 0.5 kg, 2.25 lt, etc.)
            </Text>
          </View>
        </View>

        {/* Do you want to have a calculation field to calculate product
                quantity? */}
        <View style={styles.cbView}>
          <Checkbox
            status={checkBoxchecked ? "checked" : "unchecked"}
            onPress={() => {
              setCheckBoxchecked(!checkBoxchecked);
            }}
          />
          <View style={styles.cbRightView}>
            <Text style={styles.cbText}>
              Do you want to have a calculation field to calculate product
              quantity?
            </Text>
            <Text style={styles.cbPlaceHolder}>
              (e.g 10 * 10 = 100 sq.ft, 10 * 10 * 10 = 1000 cb.ft, etc )
            </Text>
          </View>
        </View>

        {/* Track Inventory? */}
        <View style={styles.cbView}>
          <Checkbox
            status={checkBoxchecked ? "checked" : "unchecked"}
            onPress={() => {
              setCheckBoxchecked(!checkBoxchecked);
            }}
          />
          <View style={styles.cbRightView}>
            <Text style={styles.cbText}>Track Inventory? </Text>
            <Text style={styles.cbPlaceHolder}>
              (cannot be changed once product created)
            </Text>
          </View>
        </View>

        {/* Do you manage batch for this product? */}
        <View style={styles.cbView}>
          <Checkbox
            status={checkBoxchecked ? "checked" : "unchecked"}
            onPress={() => {
              setCheckBoxchecked(!checkBoxchecked);
            }}
          />
          <View style={styles.cbRightView}>
            <Text style={styles.cbText}>
              Do you manage batch for this product?{" "}
            </Text>
            <Text style={styles.cbPlaceHolder}>
              (cannot be changed once product created)
            </Text>
          </View>
        </View>

        {/* Do you manage serial number for this product? */}
        <View style={styles.cbView}>
          <Checkbox
            status={checkBoxchecked ? "checked" : "unchecked"}
            onPress={() => {
              setCheckBoxchecked(!checkBoxchecked);
            }}
          />
          <View style={styles.cbRightView}>
            <Text style={styles.cbText}>
              Do you manage serial number for this product?{" "}
            </Text>
            <Text style={styles.cbPlaceHolder}>
              (cannot be changed once product created)
            </Text>
          </View>
        </View>

        {/* Do you assemble this product? */}
        <View style={styles.cbView}>
          <Checkbox
            status={checkBoxchecked ? "checked" : "unchecked"}
            onPress={() => {
              setCheckBoxchecked(!checkBoxchecked);
            }}
          />
          <View style={styles.cbRightView}>
            <Text style={styles.cbText}>Do you assemble this product? </Text>
            <Text style={styles.cbPlaceHolder}>
              (cannot be changed once checked on and on saved)
            </Text>
          </View>
        </View>

        {/* Minimum Stock Level */}
        <View style={styles.singleView}>
          <View style={styles.borderedInput}>
            <TextInput
              style={styles.textInput}
              placeholder="Minimum Stock Level (Per Warehouse)"
            />
          </View>
          <Text style={styles.placeholder}>Minimum Stock Level</Text>
        </View>

        {/* Inventory Account */}
        <View style={[styles.innerView, { flex: 0.5 }]}>
          <TouchableWithoutFeedback>
            <View>
              <View style={styles.borderedInput}>
                <Picker
                  selectedValue={selectedCountry}
                  onValueChange={(itemValue, itemIndex) =>
                    setSelectedCountry(itemValue)
                  }
                >
                  <Picker.Item label="Inventory" value="Pk" />
                  <Picker.Item label="India" value="Ind" />
                </Picker>
              </View>
              <Text style={styles.placeholder}>Inventory Account</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        {/* End of Views */}
      </View>
    </ScrollView>
  );
}

function CustomFields() {
  const [selectedCountry, setSelectedCountry] = useState();

  return (
    <ScrollView style={GlobalStyles.outSideContainer}>
      <View style={styles.container}>
        <StatusBar backgroundColor={Colors.primary} />
        <CardView>
          <Text style={styles.addFields}>Add Fields</Text>
          <View style={styles.lineView} />

          {/* Field */}
          <View style={styles.innerView}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedCountry}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedCountry(itemValue)
                    }
                  >
                    <Picker.Item label="Field" value="Pk" />
                    <Picker.Item label="India" value="Ind" />
                  </Picker>
                </View>
                <Text style={styles.placeholder}>Field</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Action Buttons */}
          <View style={GlobalStyles.flexDirRow}>
            <Button
              color={Colors.white}
              style={styles.button}
              onPress={() => console.log("pressed!")}
            >
              Add
            </Button>
            <Button
              color={Colors.white}
              style={[
                styles.button,
                { marginLeft: 20, backgroundColor: "tomato" },
              ]}
              onPress={() => console.log("pressed!")}
            >
              Cancel
            </Button>
          </View>
        </CardView>

        <CardView>
          <Text style={styles.addFields}>Added Fields</Text>
          <View style={styles.lineView} />
        </CardView>

        {/* End of Views */}
      </View>
    </ScrollView>
  );
}

function Detail() {
  const [selectedCountry, setSelectedCountry] = useState();
  return (
    <ScrollView>
      <SafeAreaView>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.primary} />

          {/* Product Category */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedCountry}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedCountry(itemValue)
                    }
                  >
                    <Picker.Item label="Select" value="Pk" />
                    <Picker.Item label="India" value="Ind" />
                  </Picker>
                </View>
                <Text style={styles.placeholder}>Product Category</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Product Short Name */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Product Short Name"
              />
            </View>
            <Text style={styles.placeholder}>Product Short Name</Text>
          </View>

          {/* SKU */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="SKU" />
            </View>
            <Text style={styles.placeholder}>SKU</Text>
          </View>

          {/* Barcode */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Product Barcode"
              />
            </View>
            <Text style={styles.placeholder}>Product Barcode</Text>
          </View>

          {/* Brand */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedCountry}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedCountry(itemValue)
                    }
                  >
                    <Picker.Item label="Pakistan" value="Pk" />
                    <Picker.Item label="India" value="Ind" />
                  </Picker>
                </View>
                <Text style={styles.placeholder}>Brand</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Model Number */}
          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Product Model Number"
              />
            </View>
            <Text style={styles.placeholder}>Product Model Number</Text>
          </View>

          {/* HS Code */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedCountry}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedCountry(itemValue)
                    }
                  >
                    <Picker.Item label="HS Code" value="Pk" />
                    <Picker.Item label="India" value="Ind" />
                  </Picker>
                </View>
                <Text style={styles.placeholder}>HS Code</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Short description*/}
          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Short description"
              />
            </View>
            <Text style={styles.placeholder}>Short description</Text>
          </View>

          {/* Description */}
          <View style={styles.innerView}>
            <View style={[styles.borderedInput, { height: 70 }]}>
              <TextInput
                style={styles.textInput}
                placeholder="Description"
                multiline
              />
            </View>
            <Text style={styles.placeholder}>Description</Text>
          </View>

          {/* End of Views */}
        </View>
      </SafeAreaView>
    </ScrollView>
  );
}

function GeneralDetail() {
  const [selectedLanguage, setSelectedLanguage] = useState();
  const [checked, setChecked] = React.useState("first");

  const [checkBoxchecked, setCheckBoxchecked] = React.useState(true);
  return (
    <ScrollView>
      <SafeAreaView>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.primary} />

          <View style={GlobalStyles.flexDirRow}>
            <RadioButton
              value="product"
              status={checked === "product" ? "checked" : "unchecked"}
              onPress={() => setChecked("product")}
            />
            <Text style={styles.radioText}>Product</Text>
            <RadioButton
              value="services"
              status={checked === "services" ? "checked" : "unchecked"}
              onPress={() => setChecked("services")}
            />
            <Text style={styles.radioText}>Services</Text>

            <RadioButton
              value="productVariant"
              status={checked === "productVariant" ? "checked" : "unchecked"}
              onPress={() => setChecked("productVariant")}
            />
            <Text style={styles.radioText}>Product Variant</Text>
          </View>

          <View style={GlobalStyles.flexDirRow}>
            {/*Series */}

            <View style={[styles.innerView, { flex: 0.5 }]}>
              <TouchableWithoutFeedback>
                <View>
                  <View style={styles.borderedInput}>
                    <Picker
                      selectedValue={selectedLanguage}
                      onValueChange={(itemValue, itemIndex) =>
                        setSelectedLanguage(itemValue)
                      }
                    >
                      <Picker.Item label="Java" value="java" />
                      <Picker.Item label="JavaScript" value="js" />
                    </Picker>

                    {/* <CaretDown/> */}
                  </View>
                  <Text style={styles.placeholder}>Series</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            {/* Code */}

            <View style={[styles.innerView, { marginLeft: 10 }]}>
              <View style={styles.borderedInput}>
                <TextInput style={styles.textInput} placeholder="SAQ12000" />
              </View>
              <Text style={styles.placeholder}>Code</Text>
            </View>
          </View>

          {/* Name */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="Name" />
            </View>
            <Text style={styles.placeholder}>Name</Text>
          </View>

          {/* Unit of measurement */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Unit of Measurement"
              />
            </View>
            <Text style={styles.placeholder}>Unit of measurement</Text>
          </View>

          {/* do you purchase this product */}

          <View style={GlobalStyles.flexDirRow}>
            <Checkbox
              status={checkBoxchecked ? "checked" : "unchecked"}
              onPress={() => {
                setCheckBoxchecked(!checkBoxchecked);
              }}
            />
            <Text style={{ textAlignVertical: "center", fontSize: 16 }}>
              Do you purchase this product?
            </Text>
          </View>

          {/* Purchase Price */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="0" />
            </View>
            <Text style={styles.placeholder}>Purchase Price (PKR)</Text>
          </View>

          {/* <Input
            label="Hello"
            outlineColor={Colors.black}
            mode="outlined"
            selectionColor={Colors.main}
            underlineColorAndroid={Colors.main}
            placeholder="Type Something"
            theme={{ colors: { primary: Colors.main } }}
            underlineColor={Colors.main}
            style={{
              backgroundColor: Colors.white,
              borderBottomColor: Colors.white,
            }}
          /> */}

          {/* Expense Account */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Cost of Goods Sold" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>Expense Account</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Purchase Discount Account */}

          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Discounts Received" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>
                  Purchase Discount Account
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <Button
            color={Colors.white}
            style={styles.button}
            onPress={() => console.log("pressed!")}
          >
            Add Tax
          </Button>

          {/* do you purchase this product */}

          <View style={GlobalStyles.flexDirRow}>
            <Checkbox
              status={checkBoxchecked ? "checked" : "unchecked"}
              onPress={() => {
                setCheckBoxchecked(!checkBoxchecked);
              }}
            />
            <Text
              style={{
                textAlignVertical: "center",
                fontSize: 16,
                color: Colors.black,
              }}
            >
              Do you sale this product?
            </Text>
          </View>

          {/* Sale Price */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="0" />
            </View>
            <Text style={styles.placeholder}>Sale Price (PKR)</Text>
          </View>

          {/* Sale Account */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Sales" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>Sale Account</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Sale Discount Account */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Dicounts" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>Sale Discount Account</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <Button
            color={Colors.white}
            style={styles.button}
            onPress={() => console.log("pressed!")}
          >
            Add Tax
          </Button>

          {/* Maximum Retail Price */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="0" />
            </View>
            <Text style={styles.placeholder}>Maximum Retail Price (PKR)</Text>
          </View>

          <View style={GlobalStyles.flexDirRow}>
            <Checkbox
              status={checkBoxchecked ? "checked" : "unchecked"}
              onPress={() => {
                setCheckBoxchecked(!checkBoxchecked);
              }}
            />
            <Text
              style={{
                textAlignVertical: "center",
                fontSize: 16,
                color: Colors.black,
              }}
            >
              Is retail price exclusive of tax?
            </Text>
          </View>
          {/* End of Views */}
        </View>
      </SafeAreaView>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  addFields: {
    fontWeight: "bold",
    fontSize: 17,
  },
  amount: {
    textAlignVertical: "center",
    fontSize: 12,
    fontWeight: "bold",
  },
  borderedInput: {
    height: 50,
    borderColor: Colors.black,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  bottomContent: {
    textAlign: "center",
    fontSize: 13,
  },
  button: {
    backgroundColor: Colors.primary,
    marginVertical: 10,
    flex: 1,
  },
  cbPlaceHolder: {
    color: Colors.darkGray,
  },
  cbRightView: {
    marginLeft: 10,
  },
  cbText: {
    textAlignVertical: "center",
    fontSize: 16,
  },
  cbView: {
    marginBottom: 10,
    flexDirection: "row",
    marginTop: 5,
  },
  cardInnerView: {
    paddingVertical: 5,
    paddingLeft: 10,
  },
  cardView: {
    borderColor: Colors.separator,
    borderWidth: 1,
    alignSelf: "flex-end",
    backgroundColor: Colors.white,
    padding: 10,
    width: 300,
    marginTop: 15,
  },
  container: {
    padding: 10,
    backgroundColor: Colors.white,
    flex: 1,
  },
  date: {
    fontSize: 15,
    textAlignVertical: "center",
    flex: 1,
    marginHorizontal: 10,
  },
  detailSubView: {},
  grayedText: {
    color: Colors.gray,
    fontSize: 15,
    textAlign: "right",
    marginRight: 5,
  },
  innerView: {
    flex: 1,
  },
  leftContent: {
    color: Colors.textGreen,
    fontSize: 15,
    flex: 1,
  },
  lineView: {
    height: 1,
    backgroundColor: Colors.separator,
    marginTop: 20,
    marginBottom: 10,
  },
  nonChangable: {
    color: Colors.darkGray,
    marginLeft: 20,
  },
  percentTxt: {
    backgroundColor: Colors.gray,
    width: 30,
    textAlign: "center",
    fontSize: 18,
    textAlignVertical: "center",
    marginLeft: 7,
  },
  placeholder: {
    position: "absolute",
    backgroundColor: "white",
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  radioText: {
    textAlignVertical: "center",
    marginRight: 5,
  },
  rowView: {
    flexDirection: "row",
    marginVertical: 15,
  },
  rightContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
  },
  rightBorderedContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    height: 30,
  },
  singleView: {},
  searchView: {
    marginTop: 10,
  },
  searchInput: {
    fontSize: 16,
    flex: 1,
  },
  searchLine: {
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    marginTop: 5,
  },
  separator: {
    backgroundColor: Colors.separator,
    width: 1,
    marginHorizontal: 7,
  },
  separatorBlack: {
    backgroundColor: Colors.black,
    height: 1,
    marginTop: 5,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 7,
  },
  topContent: {
    color: Colors.textGreen,
    fontSize: 13,
    fontWeight: "bold",
    marginRight: 10,
    textAlign: "center",
    marginBottom: 3,
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 0.96,
  },
});

export default AddProduct;

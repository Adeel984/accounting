import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
  TouchableWithoutFeedback,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { GlobalStyles } from "../Shared/GlobalStyles";
import CaretDown from "../SharedViews/CaretDown";
import { Picker } from "@react-native-picker/picker";
import { RadioButton, Checkbox, Button } from "react-native-paper";

function AddTax() {
  const [selectedCountry, setSelectedCountry] = useState();
  const [checkBoxchecked, setCheckBoxchecked] = React.useState(true);
  return (
    <ScrollView style={GlobalStyles.outSideContainer}>
      <SafeAreaView>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.primary} />

          {/* Name */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="Name" />
            </View>
            <Text style={styles.placeholder}>Name</Text>
          </View>

          {/* Abbreviation */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="Abbreviation" />
            </View>
            <Text style={styles.placeholder}>Abbreviation</Text>
          </View>

          {/* Rate */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Rate"
                keyboardType="numeric"
              />
            </View>
            <Text style={styles.placeholder}>Rate</Text>
          </View>

          {/* Tax collected */}

          <Text style={styles.selectFollowing}>
            Please select the following
          </Text>

          <View style={styles.cbView}>
            <Checkbox
              status={checkBoxchecked ? "checked" : "unchecked"}
              onPress={() => {
                setCheckBoxchecked(!checkBoxchecked);
              }}
            />
            <Text style={styles.cbText}>Tax collected on sales</Text>
          </View>

          {/* Tax collected on sales */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedCountry}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedCountry(itemValue)
                    }
                  >
                    <Picker.Item label="Account" value="Pk" />
                    <Picker.Item label="India" value="Ind" />
                  </Picker>
                </View>
                <Text style={styles.placeholder}>Account</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Tax paid on purchase */}

          <Text style={styles.selectFollowing}>
            Please select the following
          </Text>

          <View style={styles.cbView}>
            <Checkbox
              status={checkBoxchecked ? "checked" : "unchecked"}
              onPress={() => {
                setCheckBoxchecked(!checkBoxchecked);
              }}
            />
            <Text style={styles.cbText}>Tax paid on purchase</Text>
          </View>

          {/* Tax paid on purchase */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedCountry}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedCountry(itemValue)
                    }
                  >
                    <Picker.Item label="Account" value="Pk" />
                    <Picker.Item label="India" value="Ind" />
                  </Picker>
                </View>
                <Text style={styles.placeholder}>Account</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* End of Views */}
        </View>
      </SafeAreaView>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  addFields: {
    fontWeight: "bold",
    fontSize: 17,
  },
  amount: {
    textAlignVertical: "center",
    fontSize: 12,
    fontWeight: "bold",
  },
  borderedInput: {
    height: 50,
    borderColor: Colors.black,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  bottomContent: {
    textAlign: "center",
    fontSize: 13,
  },
  button: {
    backgroundColor: Colors.primary,
    marginVertical: 10,
    flex: 1,
  },
  cbPlaceHolder: {
    color: Colors.darkGray,
  },
  cbRightView: {
    marginLeft: 10,
  },
  cbText: {
    textAlignVertical: "center",
    fontSize: 16,
  },
  cbView: {
    marginBottom: 10,
    flexDirection: "row",
    marginTop: 5,
  },
  cardInnerView: {
    paddingVertical: 5,
    paddingLeft: 10,
  },
  cardView: {
    borderColor: Colors.separator,
    borderWidth: 1,
    alignSelf: "flex-end",
    backgroundColor: Colors.white,
    padding: 10,
    width: 300,
    marginTop: 15,
  },
  container: {
    padding: 10,
    backgroundColor: Colors.white,
    flex: 1,
  },
  date: {
    fontSize: 15,
    textAlignVertical: "center",
    flex: 1,
    marginHorizontal: 10,
  },
  detailSubView: {},
  grayedText: {
    color: Colors.gray,
    fontSize: 15,
    textAlign: "right",
    marginRight: 5,
  },
  innerView: {
    flex: 1,
  },
  leftContent: {
    color: Colors.textGreen,
    fontSize: 15,
    flex: 1,
  },
  lineView: {
    height: 1,
    backgroundColor: Colors.separator,
    marginTop: 20,
    marginBottom: 10,
  },
  nonChangable: {
    color: Colors.darkGray,
    marginLeft: 20,
  },
  percentTxt: {
    backgroundColor: Colors.gray,
    width: 30,
    textAlign: "center",
    fontSize: 18,
    textAlignVertical: "center",
    marginLeft: 7,
  },
  placeholder: {
    position: "absolute",
    backgroundColor: "white",
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  radioText: {
    textAlignVertical: "center",
    marginRight: 5,
  },
  rowView: {
    flexDirection: "row",
    marginVertical: 15,
  },
  rightContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
  },
  rightBorderedContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    height: 30,
  },
  singleView: {},
  searchView: {
    marginTop: 10,
  },
  searchInput: {
    fontSize: 16,
    flex: 1,
  },
  searchLine: {
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    marginTop: 5,
  },
  selectFollowing: {
    marginTop: 15,
    marginLeft: 10,
    fontSize: 15,
  },
  separator: {
    backgroundColor: Colors.separator,
    width: 1,
    marginHorizontal: 7,
  },
  separatorBlack: {
    backgroundColor: Colors.black,
    height: 1,
    marginTop: 5,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 7,
  },
  topContent: {
    color: Colors.textGreen,
    fontSize: 13,
    fontWeight: "bold",
    marginRight: 10,
    textAlign: "center",
    marginBottom: 3,
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 0.96,
  },
});

export default AddTax;

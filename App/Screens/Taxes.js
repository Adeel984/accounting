import React from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import CardView from "../SharedViews/CardView";
import FlatButton from "../SharedViews/Button";

function Taxes({ navigation }) {
  React.useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      console.log("onFocused");
      // do something
    });

    return unsubscribe;
  }, [navigation]);

  const DATA = [
    {
      id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
      title: "Raza",
    },
  ];

  const onSaleQuotationAdd = () => {
    navigation.navigate("add");
  };

  const renderItem = () => (
    <CardView>
      <Text style={styles.userName}>Income Tax</Text>

      <Text style={[styles.rightContent, { marginVertical: 10 }]}>INC</Text>

      <View style={styles.rowDirection}>
        <Text style={styles.leftContent}>Rate</Text>
        <Text style={styles.rightContent}>3.50</Text>
      </View>
    </CardView>
  );

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.primary} />

      <View style={styles.searchView}>
        <MaterialCommunityIcons name="magnify" size={26} color="black" />
        <TextInput placeholder="Search" style={styles.searchInput} />
      </View>

      <FlatList
        data={DATA}
        style={styles.listView}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  boldFont: {
    fontWeight: "bold",
    fontSize: 17,
    color: Colors.black,
  },
  container: {
    margin: 10,
  },
  leftContent: {
    color: Colors.green,
    fontSize: 13,
    fontWeight: "bold",
    marginRight: 10,
  },
  listView: {
    backgroundColor: Colors.lightGray,
    marginTop: 2,
  },
  refNumber: {
    textAlignVertical: "center",
    fontSize: 11,
  },
  rightContent: {
    color: Colors.black,
    fontSize: 13,
  },
  searchView: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 10,
  },
  searchInput: {
    fontSize: 16,
    marginLeft: 10,
    borderBottomColor: Colors.gray,
    borderBottomWidth: 0.8,
    flex: 1,
    height: 45,
  },
  status: {
    color: "tomato",
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 1,
  },
  rowDirection: {
    flexDirection: "row",
    marginBottom: 5,
  },
});

export default Taxes;

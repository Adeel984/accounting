import React from "react";
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  FlatList,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import CardView from "../SharedViews/CardView";
import { GlobalStyles } from "../Shared/GlobalStyles";

function Products() {
  const DATA = [
    {
      id: "bd7acbea- c1b1-46c2-aed5-3ad53abb28ba",
      title: "Raza",
    },
  ];

  const renderItem = () => (
    <CardView>
      <View style={GlobalStyles.flexDirRow}>
        <Image
          style={styles.img}
          source={{
            uri: "https://reactnative.dev/img/tiny_logo.png",
          }}
        />
        <View style={GlobalStyles.flex1}>
          <View style={GlobalStyles.flexDirRow}>
            <Text style={styles.userName}>Raza</Text>
            <Text style={styles.boldFont}>Rs </Text>
            <Text style={styles.boldFont}>122.00</Text>
          </View>
          <View style={[styles.rowDirection, { marginTop: 20 }]}>
            <Text style={styles.leftContent}>Code</Text>
            <Text style={styles.rightContent}>P-000101</Text>
          </View>
        </View>
      </View>
    </CardView>
  );

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.primary} />

      <View style={styles.searchView}>
        <MaterialCommunityIcons name="magnify" size={26} color="black" />
        <TextInput placeholder="Search" style={styles.searchInput} />
      </View>

      <FlatList
        data={DATA}
        style={styles.listView}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  boldFont: {
    fontWeight: "bold",
    fontSize: 17,
    color: Colors.black,
  },
  container: {
    margin: 10,
  },
  img: {
    width: 50,
    height: 50,
    marginHorizontal: 10,
    marginTop: 10,
  },
  leftContent: {
    color: Colors.green,
    fontSize: 13,
    fontWeight: "bold",
    marginHorizontal: 10,
  },
  listView: {
    backgroundColor: Colors.lightGray,
    marginTop: 2,
  },
  refNumber: {
    textAlignVertical: "center",
    fontSize: 11,
    marginHorizontal: 4,
  },
  rightContent: {
    color: Colors.black,
    fontSize: 13,
  },
  searchView: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 10,
  },
  searchInput: {
    fontSize: 16,
    marginLeft: 10,
    borderBottomColor: Colors.gray,
    borderBottomWidth: 0.8,
    flex: 1,
    height: 45,
  },
  status: {
    color: "tomato",
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 1,
    marginLeft: 10,
  },
  rowDirection: {
    flexDirection: "row",
    marginBottom: 5,
  },
});

export default Products;

import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
  TouchableWithoutFeedback,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import CardView from "../SharedViews/CardView";
import { GlobalStyles } from "../Shared/GlobalStyles";
import FlatButton from "../SharedViews/Button";
import CaretDown from "../SharedViews/CaretDown";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Picker } from "@react-native-picker/picker";

function AddSaleDelivery({ navigation }) {
  const [selectedLanguage, setSelectedLanguage] = useState();

  const [date, setDate] = useState(new Date());
  const [showDate, setShowDate] = useState(false);

  const [expiryDate, setExpiryDate] = useState(new Date());
  const [showExpiryDate, setShowExpiryDate] = useState(false);

  const onDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowDate(Platform.OS === "ios");
    setDate(currentDate);
  };

  const onExpiryDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowExpiryDate(Platform.OS === "ios");
    setExpiryDate(currentDate);
  };

  const formatDate = () => {
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  };

  const formatExpiryDate = () => {
    return `${expiryDate.getDate()}/${
      expiryDate.getMonth() + 1
    }/${expiryDate.getFullYear()}`;
  };

  return (
    <ScrollView>
      <SafeAreaView>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.primary} />

          {/* Search View */}
          <View style={styles.searchView}>
            <View style={GlobalStyles.flexDirRow}>
              <TextInput style={styles.searchInput} placeholder="Customer" />
              <MaterialIcons name="cancel" size={24} color={Colors.darkGray} />
            </View>
            <View style={styles.searchLine} />
          </View>

          <View style={GlobalStyles.flexDirRow}>
            {/*Series */}

            <View style={[styles.innerView, { flex: 0.5 }]}>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Java" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>Series</Text>
              </View>
            </View>

            {/* Auto Generated Number */}

            <View style={[styles.innerView, { marginLeft: 10 }]}>
              <View style={styles.borderedInput}>
                <TextInput style={styles.textInput} placeholder="SAQ12000" />
              </View>
              <Text style={styles.placeholder}>Auto Generated Number</Text>
            </View>
          </View>

          {/* Line 2 */}

          <View style={styles.innerView}>
            {/* Date */}

            <TouchableWithoutFeedback onPress={() => setShowDate(true)}>
              <View style={[styles.borderedInput, { flexDirection: "row" }]}>
                <Text style={styles.date}>{formatDate()}</Text>

                {showDate && (
                  <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode="date"
                    display="calendar"
                    onChange={onDateChange}
                  />
                )}

                <CaretDown />
              </View>
            </TouchableWithoutFeedback>

            <Text style={styles.placeholder}>Date</Text>
          </View>

          {/* Warehouse */}

          <View style={[styles.innerView, { flex: 0.5 }]}>
            <View>
              <View style={styles.borderedInput}>
                <Picker
                  selectedValue={selectedLanguage}
                  onValueChange={(itemValue, itemIndex) =>
                    setSelectedLanguage(itemValue)
                  }
                >
                  <Picker.Item label="Java" value="java" />
                  <Picker.Item label="JavaScript" value="js" />
                </Picker>

                {/* <CaretDown/> */}
              </View>
              <Text style={styles.placeholder}>Warehouse</Text>
            </View>
          </View>

          {/* Refference */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insert refference"
              />
            </View>
            <Text style={styles.placeholder}>Refference</Text>
          </View>

          {/* Shipping Address */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Please insert shipping address"
              />
            </View>
            <Text style={styles.placeholder}>Shipping Address</Text>
          </View>

          {/* Comments */}

          <View style={[styles.singleView, { marginBottom: 10 }]}>
            <View style={[styles.borderedInput, { height: 70 }]}>
              <TextInput
                multiline
                style={styles.textInput}
                placeholder="Please insert Comments"
              />
            </View>
            <Text style={styles.placeholder}>Comments</Text>
          </View>

          <FlatButton text="Upload Attachment" />

          <FlatButton
            backgroundColor={Colors.primary}
            text="Add Product Detail"
            onPress={() => navigation.navigate("Add Product Detail")}
          />
        </View>
      </SafeAreaView>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  amount: {
    textAlignVertical: "center",
    fontSize: 12,
    fontWeight: "bold",
  },
  borderedInput: {
    height: 50,
    borderColor: Colors.black,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  bottomContent: {
    textAlign: "center",
    fontSize: 13,
  },
  cardInnerView: {
    paddingVertical: 5,
    paddingLeft: 10,
  },
  cardView: {
    borderColor: Colors.separator,
    borderWidth: 1,
    alignSelf: "flex-end",
    backgroundColor: Colors.white,
    padding: 10,
    width: 280,
    marginTop: 15,
  },
  container: {
    margin: 10,
  },
  date: {
    fontSize: 15,
    textAlignVertical: "center",
    flex: 1,
    marginHorizontal: 10,
  },
  detailSubView: {},
  grayedText: {
    color: Colors.gray,
    fontSize: 15,
    textAlign: "right",
    marginRight: 5,
  },
  innerView: {
    flex: 1,
  },
  leftContent: {
    color: Colors.textGreen,
    fontSize: 15,
    flex: 1,
  },
  placeholder: {
    position: "absolute",
    backgroundColor: "white",
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  rowView: {
    flexDirection: "row",
    marginVertical: 15,
  },
  rightContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
  },
  singleView: {},
  searchView: {
    marginTop: 10,
  },
  searchInput: {
    fontSize: 16,
    flex: 1,
  },
  searchLine: {
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    marginTop: 5,
  },
  separator: {
    backgroundColor: Colors.separator,
    width: 1,
    marginHorizontal: 7,
  },
  separatorBlack: {
    backgroundColor: Colors.black,
    height: 1,
    marginTop: 5,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 7,
  },
  topContent: {
    color: Colors.textGreen,
    fontSize: 13,
    fontWeight: "bold",
    marginRight: 10,
    textAlign: "center",
    marginBottom: 3,
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 0.96,
  },
});

export default AddSaleDelivery;

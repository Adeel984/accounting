import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
  TouchableWithoutFeedback,
} from "react-native";
import { Colors } from "../Shared/Colors";
import { MaterialIcons } from "@expo/vector-icons";
import { GlobalStyles } from "../Shared/GlobalStyles";
import CaretDown from "../SharedViews/CaretDown";
import { Picker } from "@react-native-picker/picker";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

const Tab = createMaterialTopTabNavigator();

export default function AddVendor() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Genderal" component={GenetalDetail} />
      <Tab.Screen name="Address" component={Address} />
    </Tab.Navigator>
  );
}

function Address() {
  const [selectedCountry, setSelectedCountry] = useState();
  return (
    <ScrollView>
      <SafeAreaView>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.primary} />
          {/* Address Line 1 */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Address Line 1"
              />
            </View>
            <Text style={styles.placeholder}>Address Line 1</Text>
          </View>
          {/* Address Line 2 */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Address Line 2"
              />
            </View>
            <Text style={styles.placeholder}>Address Line 2</Text>
          </View>
          {/* City */}
          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="City" />
            </View>
            <Text style={styles.placeholder}>City</Text>
          </View>
          <View style={GlobalStyles.flexDirRow}>
            {/* State */}
            <View style={[styles.innerView, { flex: 0.5 }]}>
              <View>
                <View style={styles.borderedInput}>
                  <TextInput style={styles.textInput} placeholder="State" />
                </View>
                <Text style={styles.placeholder}>State</Text>
              </View>
            </View>
            {/* Zip */}
            <View style={[styles.innerView, { marginLeft: 10, flex: 0.5 }]}>
              <View style={styles.borderedInput}>
                <TextInput style={styles.textInput} placeholder="Zip" />
              </View>
              <Text style={styles.placeholder}>Zip</Text>
            </View>
          </View>

          {/* Country */}

          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedCountry}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedCountry(itemValue)
                    }
                  >
                    <Picker.Item label="Pakistan" value="Pk" />
                    <Picker.Item label="India" value="Ind" />
                  </Picker>
                </View>
                <Text style={styles.placeholder}>Country</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Email */}
          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Email"
                keyboardType="email-address"
              />
            </View>
            <Text style={styles.placeholder}>Email</Text>
          </View>

          {/* Contact Person */}
          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Contact Person"
              />
            </View>
            <Text style={styles.placeholder}>Contact Person</Text>
          </View>

          {/* Phone */}
          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Phone"
                keyboardType="phone-pad"
              />
            </View>
            <Text style={styles.placeholder}>Phone</Text>
          </View>

          {/* Fax */}
          <View style={styles.innerView}>
            <View style={styles.borderedInput}>
              <TextInput
                style={styles.textInput}
                placeholder="Fax"
                keyboardType="phone-pad"
              />
            </View>
            <Text style={styles.placeholder}>Fax</Text>
          </View>

          <View style={GlobalStyles.flexDirRow}>
            {/* NTN */}
            <View style={[styles.innerView, { flex: 0.5 }]}>
              <View>
                <View style={styles.borderedInput}>
                  <TextInput style={styles.textInput} placeholder="NTN" />
                </View>
                <Text style={styles.placeholder}>NTN</Text>
              </View>
            </View>
            {/* CNIC */}
            <View style={[styles.innerView, { marginLeft: 10, flex: 0.5 }]}>
              <View style={styles.borderedInput}>
                <TextInput style={styles.textInput} placeholder="CNIC" />
              </View>
              <Text style={styles.placeholder}>CNIC</Text>
            </View>
          </View>

          {/* End of Views */}
        </View>
      </SafeAreaView>
    </ScrollView>
  );
}

function GenetalDetail() {
  const [selectedLanguage, setSelectedLanguage] = useState();

  return (
    <ScrollView>
      <SafeAreaView>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.primary} />

          <View style={GlobalStyles.flexDirRow}>
            {/*Series */}

            <View style={[styles.innerView, { flex: 0.5 }]}>
              <TouchableWithoutFeedback>
                <View>
                  <View style={styles.borderedInput}>
                    <Picker
                      selectedValue={selectedLanguage}
                      onValueChange={(itemValue, itemIndex) =>
                        setSelectedLanguage(itemValue)
                      }
                    >
                      <Picker.Item label="Java" value="java" />
                      <Picker.Item label="JavaScript" value="js" />
                    </Picker>

                    {/* <CaretDown/> */}
                  </View>
                  <Text style={styles.placeholder}>Series</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            {/* Auto Generated Number */}

            <View style={[styles.innerView, { marginLeft: 10 }]}>
              <View style={styles.borderedInput}>
                <TextInput style={styles.textInput} placeholder="SAQ12000" />
              </View>
              <Text style={styles.placeholder}>Auto Generated Number</Text>
            </View>
          </View>

          {/* Name */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="Name" />
            </View>
            <Text style={styles.placeholder}>Name</Text>
          </View>

          {/* Vendor Category */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Java" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>
                <Text style={styles.placeholder}>Vendor Category</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          {/* Print Name */}

          <View style={styles.singleView}>
            <View style={styles.borderedInput}>
              <TextInput style={styles.textInput} placeholder="Print Name" />
            </View>
            <Text style={styles.placeholder}>Print Name</Text>
          </View>

          {/* Display Name */}

          <View style={[styles.singleView, { marginBottom: 10 }]}>
            <View style={styles.borderedInput}>
              <TextInput
                multiline
                style={styles.textInput}
                placeholder="Please insert display name"
              />
            </View>
            <Text style={styles.placeholder}>Comments</Text>
          </View>

          <View style={styles.lineView} />

          {/* Currency */}
          <View style={[styles.innerView, { flex: 0.5 }]}>
            <TouchableWithoutFeedback>
              <View>
                <View style={styles.borderedInput}>
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                  >
                    <Picker.Item label="Pakistani Rupees" value="PKR" />
                  </Picker>

                  {/* <CaretDown/> */}
                </View>

                <Text style={styles.placeholder}>Currency</Text>
              </View>
            </TouchableWithoutFeedback>
            <Text style={styles.nonChangable}>
              (can not be changed once vendor created)
            </Text>
          </View>

          {/* Opening PKR */}

          <View style={[styles.singleView, { marginBottom: 10 }]}>
            <View style={styles.borderedInput}>
              <TextInput
                multiline
                style={styles.textInput}
                placeholder="Enter opening balance"
                keyboardType="numeric"
              />
            </View>
            <Text style={styles.placeholder}>Opening PKR </Text>
          </View>

          {/* Credit Limit Day */}

          <View style={[styles.singleView, { marginBottom: 10 }]}>
            <View style={styles.borderedInput}>
              <TextInput
                multiline
                style={styles.textInput}
                placeholder="0"
                keyboardType="numeric"
              />
            </View>
            <Text style={styles.placeholder}>Credit Limit Day </Text>
          </View>
        </View>
      </SafeAreaView>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  amount: {
    textAlignVertical: "center",
    fontSize: 12,
    fontWeight: "bold",
  },
  borderedInput: {
    height: 50,
    borderColor: Colors.black,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  bottomContent: {
    textAlign: "center",
    fontSize: 13,
  },
  cardInnerView: {
    paddingVertical: 5,
    paddingLeft: 10,
  },
  cardView: {
    borderColor: Colors.separator,
    borderWidth: 1,
    alignSelf: "flex-end",
    backgroundColor: Colors.white,
    padding: 10,
    width: 300,
    marginTop: 15,
  },
  container: {
    padding: 10,
    backgroundColor: Colors.white,
    flex: 1,
  },
  date: {
    fontSize: 15,
    textAlignVertical: "center",
    flex: 1,
    marginHorizontal: 10,
  },
  detailSubView: {},
  grayedText: {
    color: Colors.gray,
    fontSize: 15,
    textAlign: "right",
    marginRight: 5,
  },
  innerView: {
    flex: 1,
  },
  leftContent: {
    color: Colors.textGreen,
    fontSize: 15,
    flex: 1,
  },
  lineView: {
    height: 1,
    backgroundColor: Colors.green,
    marginVertical: 20,
  },
  nonChangable: {
    color: Colors.darkGray,
    marginLeft: 20,
  },
  percentTxt: {
    backgroundColor: Colors.gray,
    width: 30,
    textAlign: "center",
    fontSize: 18,
    textAlignVertical: "center",
    marginLeft: 7,
  },
  placeholder: {
    position: "absolute",
    backgroundColor: "white",
    paddingVertical: 2,
    paddingHorizontal: 5,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.black,
    fontSize: 12,
  },
  rowView: {
    flexDirection: "row",
    marginVertical: 15,
  },
  rightContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
  },
  rightBorderedContent: {
    fontSize: 15,
    color: Colors.black,
    textAlign: "right",
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    height: 30,
  },
  singleView: {},
  searchView: {
    marginTop: 10,
  },
  searchInput: {
    fontSize: 16,
    flex: 1,
  },
  searchLine: {
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    marginTop: 5,
  },
  separator: {
    backgroundColor: Colors.separator,
    width: 1,
    marginHorizontal: 7,
  },
  separatorBlack: {
    backgroundColor: Colors.black,
    height: 1,
    marginTop: 5,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 7,
  },
  topContent: {
    color: Colors.textGreen,
    fontSize: 13,
    fontWeight: "bold",
    marginRight: 10,
    textAlign: "center",
    marginBottom: 3,
  },
  userName: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 0.96,
  },
});

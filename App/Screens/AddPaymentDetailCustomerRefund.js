import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import { MaterialIcons } from "@expo/vector-icons";
import { Colors } from "../Shared/Colors";
import { GlobalStyles } from "../Shared/GlobalStyles";
import FlatButton from "../SharedViews/Button";
import CardView from "../SharedViews/CardView";

function AddPaymentDetailCustomerRefund() {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.primary} />

      {/* Search View */}
      <View style={styles.searchView}>
        <View style={GlobalStyles.flexDirRow}>
          <TextInput style={styles.searchInput} placeholder="Payment Mode" />
          <MaterialIcons name="cancel" size={24} color={Colors.darkGray} />
        </View>
        <View style={styles.searchLine} />
      </View>

      {/* Description */}

      <TextInput
        style={styles.textInput}
        multiline
        placeholder="Document No."
      />

      {/* Price */}
      <TextInput
        style={styles.textInput}
        placeholder="Amount"
        keyboardType="numeric"
      />

      <FlatButton text="Save" onPress={() => setModalVisible(true)} />
    </View>
  );
}

const styles = StyleSheet.create({
  borderedInput: {
    height: 50,
    borderColor: Colors.darkGray,
    borderWidth: 1,
    marginTop: 15,
    borderRadius: 6,
    justifyContent: "center",
  },
  borderedInputView: {
    borderColor: Colors.gray,
    borderWidth: 1,
    height: 50,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    flex: 0.5,
  },
  container: {
    margin: 10,
  },
  headerText: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
  },
  innerView: {},
  listHeadView: {
    marginTop: 20,
  },
  minusView: {
    backgroundColor: "tomato",
    height: 20,
    width: 20,
    borderRadius: 10,
    position: "absolute",
    right: 0,
    zIndex: 100,
  },
  modalView: {
    marginVertical: 30,
    backgroundColor: "white",
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    flex: 1,
    shadowOpacity: 0.55,
    shadowRadius: 4,
    elevation: 5,
  },
  listText: {
    color: Colors.blue2,
    marginVertical: 25,
    fontWeight: "bold",
    marginHorizontal: 10,
    fontSize: 17,
  },
  OkBtn: {
    fontWeight: "800",
    fontSize: 17,
    alignSelf: "flex-end",
    color: Colors.main,
  },
  percentageVal: {
    fontSize: 18,
    textAlignVertical: "center",
    marginHorizontal: 10,
  },
  percentageView: {
    marginLeft: 10,
    width: 100,
  },
  percentageInnerView: {
    flexDirection: "row",
    flex: 1,
  },
  percentagePHolder: {
    textAlignVertical: "center",
    fontSize: 16,
  },
  searchView: {
    marginTop: 10,
    marginBottom: 15,
  },
  searchInput: {
    fontSize: 16,
    flex: 1,
  },
  searchLine: {
    borderBottomColor: Colors.black,
    borderBottomWidth: 1,
    marginTop: 5,
  },
  textInput: {
    fontSize: 16,
    paddingHorizontal: 10,
    borderColor: Colors.black,
    borderWidth: 1,
    height: 50,
    borderRadius: 6,
    marginTop: 15,
  },
  textInputWithoutBorder: {
    fontSize: 16,
    paddingHorizontal: 10,
  },
});

export default AddPaymentDetailCustomerRefund;
